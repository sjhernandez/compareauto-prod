var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require('body-parser');
const request = require('request');


const indexRouter = require('./routes/index');
const currencyRouter = require('./routes/currency');
const fullcurrencyRouter = require('./routes/fullcurrency');
// const carsRouter = require('./routes/cars');
const usersRouter = require('./routes/users');
const quoteRouter = require('./routes/quote');
const queryRouter = require('./routes/query');
const bookingsRouter = require('./routes/bookings');
const locationsRouter = require('./routes/locations');
const locationsdRouter = require('./routes/locationsd');
const jsonRouter = require('./routes/json');
const voucherRouter = require('./routes/voucher');
const cancelRouter = require('./routes/cancelbook');
const detailsRouter = require('./routes/details');
// const testdataRouter = require('./routes/testdata');
const confirmationRouter = require('./routes/confirmation');

var app = express();

// view engine setup
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");

    next();
});
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('trust_proxy', 1)
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/voucher', voucherRouter);
app.use('/cancelbook', cancelRouter);
// app.use('/testdata', testdataRouter);
app.use('/details', detailsRouter);
// app.use('/cars', carsRouter);
app.use('/query', queryRouter);
app.use('/bookings', bookingsRouter);
app.use('/json', jsonRouter);
app.use('/locations', locationsRouter);
app.use('/locationsd', locationsdRouter);
app.use('/currency', currencyRouter);
app.use('/fullcurrency', fullcurrencyRouter);
app.use('/quote', quoteRouter);
app.use('/confirmation', confirmationRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;