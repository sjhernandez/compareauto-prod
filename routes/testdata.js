var express = require('express');
var router = express.Router();
var request = require('request');
var utf8 = require('utf8');
var xml = require('xml');
var ObjTree = require('xml-objtree');
var xmlparser = require('express-xml-bodyparser');
var { testdata, localhost } = require('../modules/config');
var { sortByPrice } = require('../modules/func');
var processObj = require('../modules/processObj')

/* GET home page. */
router.get('/', function(req, res, next) {




    request.post({
            uri: 'https://api.carrentalgateway.com/xml',
            // port: 80,
            method: 'POST',
            headers: {
                'Content-Type': 'text/xml; charset=utf-8'
            },
            body: testdata
        },
        function(error, response, body) {
            console.log(response.statusCode)
                // console.log(body);
            console.log(error)
            res.set('Access-Control-Allow-Origin', localhost)
                // var jsonResponse = JSON.stringify(parseXMLToJson(body));
            var jsonResponse = parseXMLToJson(body)
            var branches = extractBody(jsonResponse);

            var carRentals = processObj(branches);
            carRentals.sort(function(obj1, obj2) {
                return obj1.price - obj2.price;

            });

            res.send(carRentals)
                // res.render('index', {
                //     data: carRentals
                // });



        });

})

function parseXMLToJson(xmlData) {
    var objTree = new ObjTree()
    objTree.attr_prefix = '_'
    var json_result = objTree.parseXML(xmlData)
    return json_result
}

function extractBody(retjson) {
    var jreturn = retjson.RS.Responses
    return jreturn
}





module.exports = router;