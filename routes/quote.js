const express = require('express')
const router = express.Router()
const request = require('request')

var rp = require('request-promise-native');
const ObjTree = require('xml-objtree')

const {
    quoteurl,
    administration,
    localhost
} = require('../modules/config');
const processObj = require('../modules/processObj')
const {

    calFormat
} = require('../modules/func');

//let locations = require('./public/assets/locations.json')

/* POST home page. */
router.post('/', function(req, res, next) {
    let pickupdate = calFormat(req.body.pickupdate);
    let pickuptime = req.body.pickuptime;
    let dropofftime = req.body.dropofftime
    let location = req.body.locnumber
    let dropofflocation = req.body.locnumber1 || location
    let country = req.body.country
    let currency = req.body.currency;
    // const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress;


    let dropoffdate = calFormat(req.body.dropoffdate);
    const pickupdatetime1 = `${pickupdate}T${pickuptime}`

    let age = req.body.age
    let requestData =
        `${administration }<Requests><VehicleRQ lang="33"pickUpDateTime="${pickupdate}T${pickuptime}"dropOffDateTime="${dropoffdate}T${dropofftime}"><PickUpLocation code="${location}"codeContext="INTERNAL_LOCATION_ID" /><DropOffLocation code="${dropofflocation}"codeContext="INTERNAL_LOCATION_ID" />  <Driver age="${age}" /><ResidenceCountry code="${country}" /></Requests></RQ>`

    let pickupdate2 = req.body.pickupdate;
    let dropoffdate2 = req.body.dropoffdate;
    //
    var options = {
        uri: `https://compareauto.herokuapp.com/currency/?currency=${currency}`,

        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true // Automatically parses the JSON string in the response
    };

    rp(options)
        .then(function(repos) {

            let xrate = repos
            request.post({
                    //  uri: 'https://api.carrentalgateway.com/xml',
                    uri: quoteurl,
                    // port: 80,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'text/xml; charset=utf-8'
                    },
                    body: requestData
                },
                function(error, response, body) {
                    console.log(response.statusCode)

                    console.log(error)
                        // res.set('Access-Control-Allow-Origin', localhost)

                    let jsonResponse = parseXMLToJson(body)

                    let branches = extractBody(jsonResponse);
                    if (branches.VehicleRS.hasOwnProperty('AvailableBranches')) {
                        let carRentals = processObj(branches, xrate, currency);
                        let uniqueLocations = extractLocations(carRentals);
                        let uniqueCompany = extractCarCompany(carRentals);
                        let carSize = extractCarSize(carRentals);

                        carRentals.sort(function(obj1, obj2) {
                            return obj1.sortprice - obj2.sortprice;

                        });

                        //  res.json(carRentals)
                        res.render('carlist-new', {
                            data: carRentals,
                            rentalcompany: uniqueCompany,
                            locations: uniqueLocations,
                            carSizes: carSize,
                            pickupdate2,
                            dropoffdate2,
                            pickuptime,
                            dropofftime,
                            pickupdatetime1,
                            xrate,
                            currency


                        });

                    }
                    res.render('error-location');

                }
            )
        })
        .catch(function(err) {

            request.post({
                    //  uri: 'https://api.carrentalgateway.com/xml',
                    uri: quoteurl,
                    // port: 80,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'text/xml; charset=utf-8'
                    },
                    body: requestData
                },
                function(error, response, body) {
                    console.log(response.statusCode)

                    console.log(error)
                    let xrate = 1;
                    let jsonResponse = parseXMLToJson(body)

                    let branches = extractBody(jsonResponse);
                    if (branches.VehicleRS.hasOwnProperty('AvailableBranches')) {
                        let carRentals = processObj(branches, 1, 'USD');
                        let uniqueLocations = extractLocations(carRentals);
                        let uniqueCompany = extractCarCompany(carRentals);
                        let carSize = extractCarSize(carRentals);

                        carRentals.sort(function(obj1, obj2) {
                            return obj1.sortprice - obj2.sortprice;

                        });

                        //  res.json(carRentals)
                        res.render('carlist-new', {
                            data: carRentals,
                            rentalcompany: uniqueCompany,
                            locations: uniqueLocations,
                            carSizes: carSize,
                            pickupdate2,
                            dropoffdate2,
                            pickuptime,
                            dropofftime,
                            xrate,
                            currency


                        });

                    }
                    res.render('error-location');

                }
            )
        });









})

function parseXMLToJson(xmlData) {
    let objTree = new ObjTree()
    objTree.attr_prefix = '_'
    let json_result = objTree.parseXML(xmlData)
    return json_result
}

function extractBody(retjson) {
    let jreturn = retjson.RS.Responses
    return jreturn
}


function extractCarCompany(carlist) {
    let companylist = {};
    let companyarray = [];
    for (const company of carlist) {
        let {
            branchName,
            branchLogo
        } = company;
        companylist.name = branchName;
        companylist.url = branchLogo;

        let copyData = Object.assign({}, companylist);
        companyarray.push(copyData);

    }

    const companylist1 = new Map();
    for (let item of companyarray) {
        let { name, url } = item
        if (!companylist1.has(name)) {
            companylist1.set(name, url)
        }

    }
    return companylist1;


}



function extractCarSize(carlist) {
    let size = [];
    for (const car of carlist) {
        let size1 = car.size;

        size.push(size1);

    }


    return [...new Set(size)];




}

function extractLocations(carlist) {
    let locations = [];
    for (const car of carlist) {
        let loc1 = car.pickupLoc;

        locations.push(loc1);

    }

    return [...new Set(locations)];




}



module.exports = router;