var express = require('express');
var router = express.Router();
var request = require('request');
var utf8 = require('utf8');
var xml = require('xml');
const {
  localhost
} = require('../modules/config');
var ObjTree = require('xml-objtree')
var xmlparser = require('express-xml-bodyparser')


 const countries = require('../public/countries');

requestData ='<RQ><Administration><Username>compareauto_website</Username><Password>pWq212/2_</Password><Agent>CAR_support</Agent></Administration><Requests><VehicleRQ lang="33"pickUpDateTime="2018-06-19T12:00:00"dropOffDateTime="2018-06-26T12:00:00"><PickUpLocation code="28"codeContext="INTERNAL_LOCATION_ID" /><DropOffLocation code="28"codeContext="INTERNAL_LOCATION_ID" />  <Driver age="30" /><ResidenceCountry code="us" /></Requests></RQ>'


/* GET home page. */
router.get('/', function(req, res, next) {
    //res.render('index', { title: 'Express' });
     request.post(
    {
      uri: 'https://api.carrentalgateway.com/xml',
      // port: 80,
      method: 'POST',
      headers: {
        'Content-Type': 'text/xml; charset=utf-8'
      },
      body: requestData
    },
    function (error, response, body) {
      console.log(response.statusCode)
      // console.log(body);
      console.log(error)
      res.set('Access-Control-Allow-Origin', localhost)
      // var jsonResponse = JSON.stringify(parseXMLToJson(body));
    var jsonResponse = parseXMLToJson(body)
    var branches = extractBody(jsonResponse);
      // res.set('Content-Type', 'text/xml');
    //   res.send(processObj(branches));
      // response.send('its working');
    //   var carRentals = processObj(branches);
//       carRentals.sort(function(obj1, obj2){
//         return obj1.price - obj2.price;
  
//    });

 // res.send(carRentals)
//    res.render('test', { data: carRentals });

res.json(branches);

 });

    }
  )
  var combinedVehicles = [];

  function processObj (jsonObject) {
    var availableBranches = jsonObject.VehicleRS.AvailableBranches.AvailableBranch;
     var branchLocation = jsonObject.VehicleRS.PickUpLocation._locationName;
     var carsInBranches = new Object();
      // carsInBranches.branchName = jsonObject.VehicleRS.PickUpLocation._locationName
    for (let branch of availableBranches) {
     
      carsInBranches.branchLogo =  (branch.PickUpBranch.Supplier.Logo._logo) ? branch.PickUpBranch.Supplier.Logo._logo : branch.PickUpBranch.Supplier.Logo._logoSvg;
      carsInBranches.branchName = branch.PickUpBranch.Supplier._name;
      carsInBranches.dropoffInfo = branch.DropOffBranch;
      carsInBranches.pickupInfo = branch.PickUpBranch;
      carsInBranches.pickupLoc = branch.PickUpBranch._vehicleAtName;
      var cars = branch.Vehicles.Vehicle;
  
      if (Array.isArray(cars)) {
          for (let car of cars) {
            carsInBranches.packages = car.Packages
            carsInBranches.vid = car.VehicleInfo._id
            carsInBranches.transmission = car.VehicleInfo._transmission
            carsInBranches.airco = car.VehicleInfo._airco
            carsInBranches.name = car.VehicleInfo._name
            carsInBranches.size = car.VehicleInfo._subGroup
            carsInBranches.doors = car.VehicleInfo._doors
            carsInBranches.seats = car.VehicleInfo._seats
            carsInBranches.bigSuitCases = car.VehicleInfo._bigSuitcases
            carsInBranches.smallSuitCases = car.VehicleInfo._smallSuitcases
            carsInBranches.carUrl = car.VehicleInfo.Image._url
            carsInBranches.branchLocation = branchLocation
            carsInBranches.rateRef = ''
            if (Array.isArray(car.Packages.Package)) {
              carsInBranches.price = car.Packages.Package[0].VehiclePrice.SellingPrice._amount;
               carsInBranches.rentalDays = car.Packages.Package[0].VehiclePrice._rentalDays;
               carsInBranches.costPerDay = Math.floor(carsInBranches.price/carsInBranches.rentalDays); 
                carsInBranches.mileage = car.Packages.Package[0].Inclusions.Inclusion[0]._name ;
                 carsInBranches.incOne = car.Packages.Package[0].Inclusions.Inclusion[1]._name;
                 carsInBranches.incTwo = car.Packages.Package[0].Inclusions.Inclusion[2]._name;
                carsInBranches.rateRef = car.Packages.Package[0].VehiclePrice.RateReference;
               if (car.Package.packages[1]){
                    carsInBranches.mileageP2 = car.Packages.Package[1].Inclusions.Inclusion[0]._name;
                  carsInBranches.incOneP2 = car.Packages.Package[1].Inclusions.Inclusion[1]._name;
                 carsInBranches.incTwoP2 = car.Packages.Package[1].Inclusions.Inclusion[2]._name;
                    carsInBranches.rateRefP2 = car.Packages.Package[1].VehiclePrice.RateReference;
                };
      
  
          } else {
  
            carsInBranches.price = car.Packages.Package.VehiclePrice.SellingPrice._amount;
               carsInBranches.rentalDays = car.Packages.Package.VehiclePrice._rentalDays;
               carsInBranches.costPerDay = Math.floor(carsInBranches.price/carsInBranches.rentalDays);
              carsInBranches.mileage = car.Packages.Package.Inclusions.Inclusion._name;
            // if  (car.Packages.Package.Inclusions.Inclusion[1]._name) {carsInBranches.incOne = car.Packages.Package.Inclusions.Inclusion[1]._name};
            // if (car.Packages.Package.Inclusions.Inclusion[2]._name){ carsInBranches.incTwo =   car.Packages.Package.Inclusions.Inclusion[2]._name };
              carsInBranches.rateRef = car.Packages.Package.VehiclePrice.RateReference;
  
  
          }
  
          function cloneObject(obj) {
            var clone = {};
            for(var i in obj) {
                if(obj[i] != null &&  typeof(obj[i])=="object")
                    clone[i] = cloneObject(obj[i]);
                else
                    clone[i] = obj[i];
            }
            return clone;
        }
              // var copyCars = Object.assign({}, carsInBranches)
              var copyCars = cloneObject(carsInBranches)
              combinedVehicles.push(copyCars);
          
  
          }
       
      }
      
    }
    return combinedVehicles
  }
  function sortByPrice(carentals){
    carRentals.sort(function(obj1, obj2){
      return obj1.price - obj2.price;
  
  });
  return carentals;
  }

  
function parseXMLToJson (xmlData) {
    var objTree = new ObjTree()
    objTree.attr_prefix = '_'
    var json_result = objTree.parseXML(xmlData)
    return json_result
  }
  function extractBody (retjson) {
    var jreturn = retjson.RS.Responses
    return jreturn
  }



module.exports = router;

