var express = require('express');
var router = express.Router();
var request = require('request');
var utf8 = require('utf8');
var xml = require('xml');
const {
    localhost,
    administration
} = require('../modules/config');
var ObjTree = require('xml-objtree');


/* GET home page. */
router.get('/', function(req, res, next) {
    let currency = req.query.currency

    let currRequest = `${administration}<Requests><CurrencyRQ base="${currency}" /></Requests></RQ>`

    res.set('Access-Control-Allow-Origin', localhost)
    request.post({
            uri: 'https://api.carrentalgateway.com/xml',
            // port: 80,
            method: 'POST',
            headers: {
                'Content-Type': 'text/xml; charset=utf-8'
            },
            body: currRequest
        },
        function(error, response, body) {
            console.log(response.statusCode)
                // console.log(body);
            console.log(error)
            res.set('Access-Control-Allow-Origin', localhost)
                // var jsonResponse = JSON.stringify(parseXMLToJson(body));
            var jsonResponse = extractBody(parseXMLToJson(body))
            var exchangeRate = getCurrency(jsonResponse);

            res.json(exchangeRate['#text'])


            // res.render('index', {
            //     data: carRentals
            // });



        });


});

function extractBody(retjson) {
    var jreturn = retjson.RS.Responses
    return jreturn
}

function parseXMLToJson(xmlData) {
    var objTree = new ObjTree()
    objTree.attr_prefix = '_'
    var json_result = objTree.parseXML(xmlData)
    return json_result
}

function getCurrency(response) {
    if (response.CurrencyRS.Rates.hasOwnProperty('Rate')) {

        let current = response.CurrencyRS.Rates.Rate
        let [usdollar] = current.filter(function(obj) {
            return obj._currency == 'USD';
        })
        return usdollar
    }
    return 0;
}











module.exports = router;