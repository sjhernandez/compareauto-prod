var express = require('express');
var router = express.Router();
var request = require('request');
var utf8 = require('utf8');
var xml = require('xml');
let dateFormat = require('dateformat');
const {
    localhost
} = require('../modules/config');
const confirmation = require('../public/confirmation');
const extractExtras = require('../modules/extractExtras');
const extractFees = require('../modules/extractFees');





/* GET home page. */
router.get('/', function(req, res, next) {
    //res.render('index', { title: 'Express' });
    res.set('Access-Control-Allow-Origin', localhost)
    let bookingInfo = extraConfirmation(confirmation);
    // res.json(confirmation);
    // console.log(bookingInfo)
    res.render('confirmation', {
        book: bookingInfo

    });

});


const extraConfirmation = (details) => {

    let confDet = {};
    let detailsArray = [];
    let formatDate = (datetoformat) => {
        date = new Date(datetoformat);
        return dateFormat(date, "dddd, mmmm dS, yyyy, h:MM:ss TT");

    }

    let booking = details.RS.Responses.BookRS;
    confDet.resNumber = booking._reservNum;
    confDet.puDate = formatDate(booking._pickUpDateTime);
    confDet.doDate = formatDate(booking._dropOffDateTime);
    confDet.supplierReference = booking._supplierReference;
    confDet.pulocation = booking.PickUpLocation._locationName;
    confDet.dolocation = booking.DropOffLocation._locationName;
    confDet.pulong = booking.PickUpBranch._longitude;
    confDet.pulat = booking.PickUpBranch._latitude;
    confDet.openHours = booking.PickUpBranch._pickUpDayOpenHours;
    confDet.carAt = booking.PickUpBranch._vehicleAtName;
    confDet.supplierLogo = booking.PickUpBranch.Supplier.Logo._logo;
    confDet.supplierName = booking.PickUpBranch.Supplier._name;
    confDet.puInstructions = booking.PickUpBranch.SpecialInstructions;
    confDet.doInstructions = booking.DropOffBranch.SpecialInstructions;
    confDet.customerName = booking.Customer._firstName + ' ' + booking.Customer._lastName;
    confDet.customerTitle = booking.Customer._title;
    confDet.customerEmail = booking.Customer.Contact._email;
    confDet.customerPhone = booking.Customer.Contact._phone;
    confDet.extras = extractExtras(booking);
    confDet.fees = extractFees(booking);
    confDet.carImage = booking.Vehicle.VehicleInfo.Image._url;
    confDet.carClass = booking.Vehicle.VehicleInfo._subGroup;
    confDet.carName = booking.Vehicle.VehicleInfo._name
    confDet.rentalDays = booking.Vehicle.VehiclePrice._rentalDays;
    confDet.total = booking.Vehicle.VehiclePrice.SellingPrice._estimatedTotalAmount;
    confDet.payNow = booking.Vehicle.VehiclePrice.SellingPrice._profit;
    confDet.taxes = (booking.Vehicle.VehiclePrice.SellingPrice._estimatedTotalAmount - booking.Vehicle.VehiclePrice.SellingPrice._amount).toFixed(2);
    confDet.payAtPu = (booking.Vehicle.VehiclePrice.SellingPrice._estimatedTotalAmount - booking.Vehicle.VehiclePrice.SellingPrice._profit).toFixed(2);
    confDet.price = booking.Vehicle.VehiclePrice.SellingPrice._amount;
    confDet.dailyPrice = Math.floor(confDet.price / confDet.rentalDays);
    confDet.terms = extractTerms(booking.Terms.SubSection[0].Paragraph)




    return confDet;
}
const extractTerms = (terms) => {
    let termpage = ''
    for (const page of terms) {
        //   let  title = page._name;
        let html1 = page.Text['#cdata-section']
        termpage += html1;
    }
    // console.log(termpage)
    return termpage;
}


module.exports = router;