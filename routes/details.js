const express = require('express');
const router = express.Router();
const request = require('request');
const utf8 = require('utf8');
const xml = require('xml');
const dateFormat = require('dateformat');
const ObjTree = require('xml-objtree');
const xmlparser = require('express-xml-bodyparser');
const extractData = require('../modules/extractData');
const extractExtras = require('../modules/extractExtras');
const extractFees = require('../modules/extractFees');

const {
    administration,
    quoteurl,
    localhost
} = require('../modules/config');

router.get('/', function(req, res, next) {
    let xrate = req.query.xrate
    let rateRef = req.query.rateRef
    let currency = req.query.currency


    let requestData = `${administration}<Requests><RateRulesRQ><RateReference>${rateRef}</RateReference></RateRulesRQ></Requests></RQ>`


    request.post({
            uri: quoteurl,
            // port: 80,
            method: 'POST',
            headers: {
                'Content-Type': 'text/xml; charset=utf-8'
            },
            body: requestData
        },
        function(error, response, body) {
            console.log(response.statusCode)
                // console.log(body);
            console.log(error)
            res.set('Access-Control-Allow-Origin', localhost)

            let jsonResponse = parseXMLToJson(body)

            let branches = extractBody(jsonResponse);
            if (branches.hasOwnProperty('PickUpBranch')) {
                let rentalCar = extractData(branches, xrate, currency);
                let rentalExtras = extractExtras(branches, xrate, currency);
                let fees = extractFees(branches, xrate)



                //res.send(jsonResponse)
                res.render('details', {
                    rentalCar,
                    rentalExtras,
                    fees,
                    xrate,
                    currency
                });

            }
            res.render('error-location');
        });

})


function parseXMLToJson(xmlData) {
    let objTree = new ObjTree()
    objTree.attr_prefix = '_'
    let json_result = objTree.parseXML(xmlData)
    return json_result
}

function extractBody(retjson) {
    let jreturn = retjson.RS.Responses.RateRulesRS
    return jreturn
}









module.exports = router;