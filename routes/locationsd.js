var express = require('express');
var router = express.Router();
var request = require('request');
var utf8 = require('utf8');
var xml = require('xml');
var ObjTree = require('xml-objtree');
var { administration, quoteurl, localhost } = require('../modules/config');


/* get home page. */
router.post('/', function(req, res, next) {
    var location = req.body.query
    var requestData = `${administration}<Requests><LocationSearchRQ search="${location}" limit="10" lang="33" /></Requests></RQ>`

    //console.log(location)
    //console.log(requestData)

    request.post({
            uri: quoteurl,
            // port: 80,
            method: 'POST',
            headers: {
                'Content-Type': 'text/xml; charset=utf-8'
            },
            body: requestData
        },
        function(error, response, body) {
            console.log(response.statusCode);

            console.log(error);
            res.set('Access-Control-Allow-Origin', localhost);

            let jsonResponse = parseXMLToJson(body);

            let locationList = formatResponse(jsonResponse);
            console.log(locationList)
                //  var branches = extractBody(jsonResponse);
                //   //    res.send(processObj(branches));

            res.json(locationList);

        });
});





const combinedLocations = [];
var locationId, name, country;

function formatResponse(jsonobj) {

    var locations = (jsonobj.RS.Responses.LocationSearchRS.LocationList.Location) ? jsonobj.RS.Responses.LocationSearchRS.LocationList.Location : "not available"


    var loclist = {};




    loclist.id = locations._id;
    loclist.name = locations.Name;
    loclist.country = locations.Country["#text"];






    return loclist
}



function parseXMLToJson(xmlData) {
    var objTree = new ObjTree();
    objTree.attr_prefix = '_';
    var json_result = objTree.parseXML(xmlData);
    return json_result
}

function extractBody(retjson) {
    var jreturn = retjson.RS.Responses;
    return jreturn
}






module.exports = router;