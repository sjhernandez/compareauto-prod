var express = require('express');
var sys = require('util');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  //res.render('index', { title: 'Express' });
  res.render('views/index');
});

module.exports = router;