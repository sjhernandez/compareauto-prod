var express = require('express');
var router = express.Router();
var request = require('request');

var { administration, booking, localhost } = require('../modules/config');


/* get home page. */
router.get('/', function(req, res, next) {
    var resNum = req.query.resNum
    var email = req.query.email
    var requestData = `${administration}<Requests><BookingInfoRQ reservNum="${resNum}" lang="33" /></Requests></RQ>`
    console.log(resNum, email)


    request.post({
            uri: booking,
            // port: 80,
            method: 'POST',
            headers: {
                'Content-Type': 'text/xml; charset=utf-8'
            },
            body: requestData
        },
        function(error, response, body) {
            console.log(response.statusCode);

            console.log(error);

            // let jsonResponse = parseXMLToJson(body);
            // let locationList = formatResponse(jsonResponse);
            //console.log(jsonResponse)
            //  var branches = extractBody(jsonResponse);
            //   //    res.send(processObj(branches));
            res.send(body)
                // res.json(locationList);

        });
});
module.exports = router;