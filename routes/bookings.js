const express = require('express');
const router = express.Router();
const request = require('request');
const utf8 = require('utf8');
const xml = require('xml');
const ObjTree = require('xml-objtree');
const {
    administration,
    booking,
    localhost
} = require('../modules/config');
const setExtras = require('../modules/setExtras');
const extraConfirmation = require('../modules/extraConfirmation');

/* GET users listing. */
router.post('/', function(req, res, next) {

    // console.log(req.body);
    let rate = req.body.rateref;
    // console.log(rate);
    // let email = 'sam@compareautorentals.com'
    // let titleId = '1'
    // let lastName = 'Tester'
    // let firstName = 'Test'
    // let phone = '+7865555555'
    let extras = setExtras(req.body);
    // console.log(extras);
    let email = req.body.email;
    let titleId = '1';
    let lastName = req.body.lastname;
    let firstName = req.body.firstname;
    let phone = req.body.phone;
    let ccnumber = req.body.ccnumber;
    let cvv = req.body.cvv;
    let ccname = req.body.ccname
    let mexp = req.body.expirymonth;
    let yexp = req.body.expiryyear;

    // let requestData = `${administration}<Requests><BookRQ lang="33" paymentMethod="0"><RateReference>${rate}</RateReference><Customer email="${email}"titleId="${titleId}"firstName="${firstName}"lastName="${lastName}"phone="${phone}"></Customer><Extras>${extras}</Extras></BookRQ></Requests></RQ>`
    let requestData = `${administration}<Requests><BookRQ lang="33"><RateReference>${rate}</RateReference><Payment cardNumber ="${ccnumber}" cardCvv2="${cvv}" cardHolder="${ccname}" cardExpiration="${yexp}-${mexp}"/><Customer email="${email}"titleId="${titleId}"firstName="${firstName}"lastName="${lastName}"phone="${phone}"</Customer><Extras>${extras}</Extras></BookRQ></Requests></RQ>`


    // console.log(requestData)

    request.post({
            uri: booking,
            // port: 80,
            method: 'POST',
            headers: {
                'Content-Type': 'text/xml; charset=utf-8'
            },
            body: requestData
        },
        function(error, response, body) {
            console.log(response.statusCode);

            console.log(error);
            res.set('Access-Control-Allow-Origin', localhost);
            let jsonResponse = parseXMLToJson(body);
            let bookingInfo = extraConfirmation(jsonResponse)
            res.send(jsonResponse);
            console.log(response);
            console.log(body);
            // res.render('confirmation', {
            //     book: bookingInfo

            // });

        })
})

function parseXMLToJson(xmlData) {
    let objTree = new ObjTree()
    objTree.attr_prefix = '_'
    let json_result = objTree.parseXML(xmlData)
    return json_result
}






module.exports = router