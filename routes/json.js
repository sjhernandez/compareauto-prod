var express = require('express');
var router = express.Router();
var request = require('request');
var utf8 = require('utf8');
var xml = require('xml');
const {
    localhost
} = require('../modules/config');
 const countries = require('../public/countries');



/* GET home page. */
router.get('/', function(req, res, next) {
    //res.render('index', { title: 'Express' });
    res.set('Access-Control-Allow-Origin', localhost)
   res.json(countries);

});
  




module.exports = router;