<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-85516100-1"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());

  		gtag('config', 'UA-85516100-1');
	</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Compare Auto Rentals | Contact</title>
	<link rel="shortcut icon" type="image/x-icon" href="../img/favicon.png">
    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/business-casual.css" rel="stylesheet">
	
    <!-- Font Awsome -->
	<link rel="stylesheet" href="../js/font-awesome/css/font-awesome.min.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){
z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='https://v2.zopim.com/?BWafkv1UYCSBn82T47iV9AlS4AfeOAwo';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zendesk Chat Script-->
</head>

<body class="contact-page">

    <header>
		<div class="container">
			<div class="row">
				<div class="col-md-6"><a href="../index.php"><img src="../img/logo.png" /></a></div>
				<div class="col-md-6 text-right">
					<div class="header-info">
						<span class="bold">RESERVATION HOTLINE</span>
						<span class="normal">(800) 644-5687</span>
					</div>
				</div>
			</div>
		</div>
	</header>

    

    
	<div class="main-slider">
		<div class="row">
		<!-- Navigation -->
			<nav class="navbar navbar-default" role="navigation">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- navbar-brand is hidden on larger screens, but visible when the menu is collapsed -->
						<a class="navbar-brand" href="index.html">Navigation</a>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse no-padding" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li>
								<a href="../index.php">Home</a>
							</li>
							<li>
								<a href="../about.html">About</a>
							</li>
							<li>
								<a href="../services.html">Our Services</a>
							</li>
							<li>
								<a href="../contact.php" class="active">Contact</a>
							</li>						
						</ul>
					</div>
					<!-- /.navbar-collapse -->
				</div>
				<!-- /.container -->
			</nav>
		</div>
	</div>
	<div class="page-title">
		<h1 class="text-center red-color">Friend Refer</h1>
	</div>
	<div class="container">
			<form method="post" action="<?php echo $_SERVER['REQUEST_URI'];?>">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<span>Name: <span class="red-color">*</span></span>
							<input type="text" class="form-control" id="name" name="name" required>
						</div>
						<div class="form-group">
							<span>Email: <span class="red-color">*</span></span>
							<input type="email" class="form-control" id="email" name="email" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Website:(OPTIONAL) <span class="red-color">*</span></span>
							<input type="text" class="form-control" id="name" name="website">
						</div>
						<div class="form-group">
							
							  <input type="submit" style="margin-top:35px;" class="btn btn-default" name="submit" value="SUBMIT">
						</div>
					</div>
				</div>
			</form>
			<?php 
						$name = $_POST["name"];
						$email = $_POST["email"];
						$website = $_POST["website"];
						$subject = 'REFER FRIEND FORM';
						$to = 'compareautorentals@gmail.com';
						$headers='From: <$email>' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
						$message="Name: $name \r\n \nEmail: $email \r\n \nWebsite: $website";
						if(isset($_POST["submit"])) 
						{
						if(mail($to,$subject,$message,$headers))
						{
						echo "<strong>Thanks!!! Your Friend has been recommended.</strong>";
						} else 
						{
						echo "Failed to send the email";
						}
						}
			?>
	</div>
	<div class="container">
			<div class="row">
			<br>
			<legend style="font-size:40px;" class="text-center">What do I earn by recommending a friend?</legend>
			  <div class="col-md-4">
				<div class="icon-box" style="border-top-left-radius:20px;border-top-right-radius:20px;">
					<span <i class="fa fa-money" style="font-size:50px;" aria-hidden="true"></span>
					<h2>Make More Profits</h2>
					<p>You earn a certain percentage of your friend's sales determined between CompareAutoRentals, you and your friend once referred.</p>
				</div>
			  </div>
			  <div class="col-md-4">
				<div class="icon-box" style="border-top-left-radius:20px;border-top-right-radius:20px;">
					<span <i class="fa fa-link" style="font-size:50px;" aria-hidden="true"></span>
					<h2>Affiliation Chain</h2>
					<p>You can get a chain of affiliates and earn income at all times even if you are not working. Who would not want that life?</p>
				</div>
			  </div>
			  <div class="col-md-4">
				<div class="icon-box" style="border-top-left-radius:20px;border-top-right-radius:20px;">
					<span <i class="fa fa-users" style="font-size:50px;" aria-hidden="true"></i></span>
					<h2>Encourage him to start</h2>
					<p>Encourage your friend to start making money just need to do the work and you begin to notice the income.</p>
				</div>
			  </div>
			</div>
		</div>
    <!-- /.container -->

    <footer>
        <div class="container">
            <div class="row">
               <div class="col-md-3">
					<h5>COMPANY</h5>
						
							<ul>
								<li><i class="fa fa-home" aria-hidden="true"></i><a href="../index.php">  Home</a></li>
								<li><i class="fa fa-handshake-o" aria-hidden="true"></i><a href="../services.html">  Our Services</a></li>
								<li><i class="fa fa-phone" aria-hidden="true"></i><a href="../contact.php">  Contact</a></li>
								<li><i class="fa fa-briefcase" aria-hidden="true"></i><a href="../affiliate/">  Become a Affiliate (NEW)</a></li>
							</ul>
					</div>
					
	               			<div class="col-md-3">
						<h5>HELP</h5>
						
							<ul>
								<li><i class="fa fa-user-secret" aria-hidden="true"> </i><a href="../privacy_policy.html">  Privacy Policy</a></li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"> </i><a href="../terms-and-conditions.html">  Terms & Conditions</a></li>
								<li><i class="fa fa-question" aria-hidden="true"> </i><a href="../faq">  
		  FAQ</a></li>
								<li><i class="fa fa-ban" aria-hidden="true"> </i><a href="../cancellation-and-refund.php">  Cancellation</a></li>
							</ul>
				   	</div>
				   	
	               			<div class="col-md-6">
					<p>Sign up to get the latest on sales, new releases and more …</p>
					<!-- Begin MailChimp Signup Form -->
					<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
					<style type="text/css">
					/* #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; } */
					/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
					We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
					#mc_embed_signup form { padding: 0px;}
					#mc_embed_signup .button {
						width: 100%;
						border-radius: 0;
						background: #fff;
						color: #a60000;
					}
					#mc_embed_signup .button:hover {
						background: #000;
						color: #fff;
					}
					#mc_embed_signup .mc-field-group {
						width: 100%;
					}
					#mc_embed_signup #mc-embedded-subscribe-form input.mce_inline_error {
						border: 1px solid #fff;
						border-radius: 0;
					}
					#mc_embed_signup #mc-embedded-subscribe-form div.mce_inline_error {
						display: block;
						margin: 2px 0 10px 0;
						padding: 5px 10px;
						background-color: rgba(255,255,255,0.85);
						-webkit-border-radius: 0px;
						-moz-border-radius: 0px;
						border-radius: 0px;
						font-size: 14px;
						font-weight: normal;
						z-index: 1;
						color: #e85c41;
					}
					
					#mc_embed_signup div.response {
						margin: 0em 0px;
						padding: 0em 0.5em 0.5em 0px;
						font-weight: bold;
						float: left;
						top: 0;
						z-index: 1;
						width: 100%;
					}
					#mc_embed_signup input {
						border: 1px solid #fff;
						-webkit-border-radius: 0px;
						-moz-border-radius: 0px;
						border-radius: 0px;
						margin-bottom: 10px;
					}
					.white-bg a {
						color: #000;
					}
					#mc_embed_signup div#mce-responses {
						float: left;
						top: -1.4em;
						padding: 0;
						overflow: hidden;
						width: 100%;
						margin: 0 0%;
						clear: both;
					}
					#mc_embed_signup #mce-success-response {
						background: #529214;
						color: #fff;
						padding: 1em;
					}
					h2,p{
			                        text-align: center;
			                    }
			                .col-md-12{
			                        text-align: center;
			                    }
			
			                .go-up{
			                        display:inline-block;
			                        padding: 20px;
			                        background:#e85c41;
			                        border: 1px solid #e85c41;
			                        border-radius: 5px;
			                        font-size: 20px;
			                        color: #fff;
			                        cursor: pointer;
			                        position: fixed;
			                        bottom: 0;
			                        right: 0;
			                        opacity: 0.8;
			                        transition: 2s;
			                     }
			                 .go-up:active{
			                         opacity: 1;
			                         background: #a60000;
			                         transition: 0.5s;
			                    }
					</style>
					<div id="mc_embed_signup">
					<form action="//CompareAutoRentals.us15.list-manage.com/subscribe/post?u=812760ba9a91149a1d1c5ef46&amp;id=f14ce6bf37" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<div id="mc_embed_signup_scroll">
					<div class="mc-field-group">
					
					<input type="email" value="" name="EMAIL" placeholder="Enter your email" class="newslatter required email" id="mce-EMAIL">
					</div>
					
					<div id="mce-responses" class="clear">
					<div class="response" id="mce-error-response" style="display:none"></div>
					<div class="response" id="mce-success-response" style="display:none"></div>
					</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_812760ba9a91149a1d1c5ef46_f14ce6bf37" tabindex="-1" value=""></div>
					<div class="clear"><input type="submit" value="sign up" name="subscribe" id="mc-embedded-subscribe" class="white-btn button"></div>
					</div>
					</form>
					</div>
					<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
					<!--End mc_embed_signup-->
			   </div>
			   <div class="col-md-12">
					<h5>Countries to visit with us</h5>
					<ul class="col-md-4">
				<li><a href="https://compareautorentals.com/atlanta/?p_iata=ATL">Atlanta</a></li>
                        	<li><a href="https://compareautorentals.com/new-york/?p_iata=LGA">New York</a></li>
				<li><a href="https://compareautorentals.com/miami/?p_iata=MIA">Miami</a></li>
				<li><a href="https://compareautorentals.com/orlando/?p_iata=TLL">Orlando</a></li>
				<li><a href="https://compareautorentals.com/tampa/?p_iata=TPA">Tampa</a></li>
				<li><a href="https://compareautorentals.com/cancun/?p_iata=CUN">Cancun</a></li>
				<li><a href="https://compareautorentals.com/mexico/?p_iata=MEX">Mexico City</a></li>
				<li><a href="https://compareautorentals.com/lauderdale/?p_iata=FLL">Lauderdale</a></li>
					</ul>
					<ul class="col-md-4">
				<li><a href="https://compareautorentals.com/india-dehli/?p_iata=DEL">India Dehli</a></li>
				<li><a href="https://compareautorentals.com/brazil/?p_iata=DIQ">Brazil</a></li>
				<li><a href="https://compareautorentals.com/shannon-limerick/?p_iata=SNN">Shannon Limerick</a></li>
				<li><a href="https://compareautorentals.com/dublin/?p_iata=DUB">Dublin</a></li>
				<li><a href="https://compareautorentals.com/cork/?p_iata=ORK">Cork</a></li>
				<li><a href="https://compareautorentals.com/belfast-ireland-city-airport/?p_iata=BFS">Belfast Ireland</a></li>
			        <li><a href="https://compareautorentals.com/seattle/?p_iata=BFI">Seattle</a></li>
			        <li><a href="https://compareautorentals.com/san-francisco/?p_iata=SFO">San Francisco</a></li>
					</ul>
                   			<ul class="col-md-4">
				<li><a href="https://compareautorentals.com/new-jersey/?p_iata=EWR">New Jersey</a></li>
				<li><a href="https://compareautorentals.com/madrid/?p_iata=MAD">Madrid</a></li>
				<li><a href="https://compareautorentals.com/london/?p_iata=LCY">London</a></li>
				<li><a href="https://compareautorentals.com/dallas/?p_iata=DAL">Dallas</a></li>
				<li><a href="https://compareautorentals.com/chicago/?p_iata=MDW">Chicago</a></li>
				<li><a href="https://compareautorentals.com/buenos-aires/?p_iata=AEP">Buenos Aires</a></li>
				<li><a href="https://compareautorentals.com/india-mumbai/?p_iata=DEL">India Mumbai</a></li>
					</ul>
			 </div>
            </div>
        </div>
        <div class="white-bg">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<p>© Copyright COMPARE AUTO RENTALS | 2017-2018</p>
					</div>
					<div class="col-md-6 text-right">
						<ul class="footer-social-icon">
							<li><a href="https://web.facebook.com/compareautorentals/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="https://twitter.com/compareautorent/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="https://www.instagram.com/CompareAutoRentals/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
    <script>$(document).ready(function(){
		    $('.go-up').click(function(){
		        $('body,html').animate({
		            scrollTop: '0px'
		        },1000);
		    });
		
		});
	</script>
</body>

</html>
