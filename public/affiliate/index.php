<?php
	$name = $_POST["name"];
	$from = $_POST["email"];
	$phonenumber = $_POST["phone"];
	$subject ="New Affiliate";
	$to ="compareautorentals@gmail.com";
	$reply = "support.compareautorentals.com";
	$headers ='From: <$From>' . "\r\n" . 'Reply-To: <$reply>' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
	$message ="Name:    $name  \r\n  \nPhonenumber:   $phonenumber  \r\n  \Email:   $from   \r\n  \nIf we do not respond in the next 24 hours call (800) 644-5687 or (212) 726-2282";
	
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-85516100-1"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());

  		gtag('config', 'UA-85516100-1');
	</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Compare Auto Rentals | AFFILIATE</title>
	<link rel="shortcut icon" type="image/x-icon" href="../img/favicon.png">
    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="affiliate.css" rel="stylesheet">
    <!--Java Script-->
    <script src="affiliate.js" type="text/javascript"></script>
    <!-- Custom CSS -->
    <link href="../css/business-casual.css" rel="stylesheet">
	
    <!-- Font Awsome -->
	<link rel="stylesheet" href="../js/font-awesome/css/font-awesome.min.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){
z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='https://v2.zopim.com/?BWafkv1UYCSBn82T47iV9AlS4AfeOAwo';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zendesk Chat Script-->
</head>

<body class="contact-page">
    <header>
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<a href="../index.php"><img src="../img/logo.png" alt=""/></a>
				</div>
			</div>
		</div>
    </header>
	<div class="container text-center">
	
	
		<div class="row">
		<legend class="h2 text-primary">We are only successful if you are successful!!!</legend>
		<h3 class="text-info">Become part of our affiliate program. Complete the form to contact us and get more information about the benefits of being a member of <strong>Compareautorentals.com</strong> in addition to an <u>exclusive</u> Travel Agent Account.</h3>
				  <form id="contact" action="" method="post" class="align-center" style="margin-top: 40px;margin-bottom: 20px;">
				    <h4>Affiliate Form</h4>
				    <fieldset>
				      <input placeholder="Name and Last Name*" name="name" type="text" tabindex="1" required autofocus>
				    </fieldset>
				    <fieldset>
				      <input placeholder="Email Address*" name="email" type="email" tabindex="2" required>
				    </fieldset>
				    <fieldset>
				      <input placeholder="Phone Number (optional)" name="phone" type="tel" tabindex="3">
				    </fieldset>
				    <fieldset>
				      <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Submit</button>
				    </fieldset>
				       <?php
						if(isset($_POST["submit"])) 
						{
						if(mail($to,$subject,$message,$headers))
						{
						echo "Thanks!!! Your email has been send";
						} else 
						{
						echo "<strong>Failed to send the email</strong>";
						}
						}		
					?>
				  </form>
				  
  		</div>
	</div>
	<div class="container">
			<div class="row">
			<br>
			<legend style="font-size:40px;" class="text-center">What do I earn working as a Afiliate?</legend>
			  <div class="col-md-3">
				<div class="icon-box" style="border-top-left-radius:20px;border-top-right-radius:20px;">
					<i class="fa fa-credit-card-alt" style="font-size:50px;" aria-hidden="true"></i>
					<h2>Earn money</h2>
					<p>Earn a percentage determined by you and Compareautorentals.com with each sale made by your exclusive account.</p>
				</div>
			  </div>
			  <div class="col-md-3">
				<div class="icon-box" style="border-top-left-radius:20px;border-top-right-radius:20px;">
					<i class="fa fa-users" style="font-size:50px;" aria-hidden="true"></i>
					<h2>Affiliation Chain</h2>
					<p>You can get a chain of affiliates and earn income at all times even if you are not working.</p>
				</div>
			  </div>
			  <div class="col-md-3">
				<div class="icon-box" style="border-top-left-radius:20px;border-top-right-radius:20px;">
					<i class="fa fa-shopping-basket" style="font-size:50px;" aria-hidden="true"></i>
					<h2>Special offers and deals</h2>
					<p>Double your sales with unique offers and discounts that will help you make your sales easily.</p>
				</div>
			  </div>
			  <div class="col-md-3">
				<div class="icon-box" style="border-top-left-radius:20px;border-top-right-radius:20px;">
					<i class="fa fa-archive" style="font-size:50px;" aria-hidden="true"></i>
					<h2>Booking Box</h2>
					<p>CompareAutoRentals provides you with a booking box with which you can make reservations to your name directly from a web page.</p>
				</div>
			  </div>
			</div>
		</div>
	<footer>
        <div class="container">
            <div class="row">
               <div class="col-md-6 text-center">
					<h5>COMPANY</h5>
						
							<ul>
								<li><i class="fa fa-home" aria-hidden="true"></i><a href="../index.php">  Home</a></li>
								<li><i class="fa fa-h-square" aria-hidden="true"></i><a href="../bf57bb987269ecb212361468e0b10eba6bbc4a05.html">  Booking a Hotel (NEW)</a></li>
								<li><i class="fa fa-handshake-o" aria-hidden="true"></i><a href="../services.html">  Our Services</a></li>
								<li><i class="fa fa-phone" aria-hidden="true"></i><a href="../contact.php">  Contact</a></li>
								<li><i class="fa fa-briefcase" aria-hidden="true"></i><a href="../affiliate/">  Become a Affiliate (NEW)</a></li>
								<li><i class="fa fa-briefcase" aria-hidden="true"></i><a href="../travel_agent.html">  Travel Agent (NEW)</a></li>
								<li><i class="fa fa-car" aria-hidden="true"></i><a href="../luxury-car.html">  Luxury cars (NEW)</a></li>
							</ul>
					</div>
					
	               			<div class="col-md-6 text-center">
						<h5>HELP</h5>
						
							<ul>
								<li><i class="fa fa-user-secret" aria-hidden="true"> </i><a href="../privacy_policy.html">  Privacy Policy</a></li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"> </i><a href="../terms-and-conditions.html">  Terms & Conditions</a></li>
								<li><i class="fa fa-question" aria-hidden="true"> </i><a href="../faq">  
		  FAQ</a></li>
								<li><i class="fa fa-ban" aria-hidden="true"> </i><a href="../cancellation-and-refund.php">  Cancellation</a></li>
								<li><i class="fa fa-shield" aria-hidden="true"></i><a href="../payment-security.html">  Security (NEW)</a></li>
							</ul>
			   </div>
            </div>
        </div>
        <div class="white-bg">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<p>© Copyright COMPARE AUTO RENTALS | 2017-2018</p>
					</div>
					<div class="col-md-6 text-right">
						<ul class="footer-social-icon">
							<li><a href="https://web.facebook.com/compareautorentals/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="https://twitter.com/compareautorent/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="https://www.instagram.com/CompareAutoRentals/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>