$('#autocomplete').autocomplete({
 minLength: 3,
 source: loadFromAjax,
 appendTo: '#search'


});

function loadFromAjax(request, response){
$.ajax({
type: 'POST',
datatype: 'xml',
url: 'https://api.carrentalgateway.com/xml',
data: '<RQ><Administration><Username>compareauto_website</Username><Password>pWq212/2_</Password><Agent>CAR_support</Agent></Administration><LocationSearchRQ search="faro flughafen" limit="5" lang="1" /></Requests></RQ>',
success: function(data) {
            // you can format data here if necessary
            response(data);
}
});

}
$( function() {
    function log( message ) {
      $( "<div/>" ).text( message ).prependTo( "#log" );
      $( "#log" ).attr( "scrollTop", 0 );
    }
 
    $.ajax({
      url: "london.xml",
      dataType: "xml",
      success: function( xmlResponse ) {
        var data = $( "geoname", xmlResponse ).map(function() {
          return {
            value: $( "name", this ).text() + ", " +
              ( $.trim( $( "countryName", this ).text() ) || "(unknown country)" ),
            id: $( "geonameId", this ).text()
          };
        }).get();
        $( "#birds" ).autocomplete({
          source: data,
          minLength: 0,
          select: function( event, ui ) {
            log( ui.item ?
              "Selected: " + ui.item.value + ", geonameId: " + ui.item.id :
              "Nothing selected, input was " + this.value );
          }
        });
      }
    });
  } );