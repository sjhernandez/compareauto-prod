<?php
	$name = $_POST["name"];
	$from = $_POST["email"];
	$phonenumber = $_POST["phone"];
	$subject ="New Affiliate";
	$to ="compareautorentals@gmail.com";
	$reply = "support.compareautorentals.com";
	$headers ='From: <$From>' . "\r\n" . 'Reply-To: <$reply>' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
	$message ="Name:    $name  \r\n  \nPhonenumber:   $phonenumber  \r\n  \Email:   $from   \r\n  \nIf we do not respond in the next 24 hours call (800) 644-5687 or (212) 726-2282";
	
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-85516100-1"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());

  		gtag('config', 'UA-85516100-1');
	</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Compare Auto Rentals | Afiliarce</title>
	<link rel="shortcut icon" type="image/x-icon" href="../../img/favicon.png">
    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="affiliate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../css/business-casual.css" rel="stylesheet">
	
    <!-- Font Awsome -->
	<link rel="stylesheet" href="../../js/font-awesome/css/font-awesome.min.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){
z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='https://v2.zopim.com/?BWafkv1UYCSBn82T47iV9AlS4AfeOAwo';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zendesk Chat Script-->
</head>

<body class="contact-page">
    <header>
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<a href="../index.php"><img src="../../img/logo.png" alt=""/></a>
				</div>
			</div>
		</div>
    </header>
	<div class="container text-center">
	
	
		<div class="row">
		<legend class="h2 text-primary">Nosotros somos exitosos si usted es exitoso!!!</legend>
		<h3 class="text-info">Conviertete en parte de nuestro programa de afiliados. Complete el formulario para contactarnos y obtener más informacion sobre los beneficios de ser miembro en <strong>Compareautorentals.com</strong>.</h3>
				  <form id="contact" action="" method="post" class="align-center">
				    <h4>Planilla de Afiliación</h4>
				    <fieldset>
				      <input placeholder="Nombre y Apellidos*" name="name" type="text" tabindex="1" required autofocus>
				    </fieldset>
				    <fieldset>
				      <input placeholder="Direccion de Correo*" name="email" type="email" tabindex="2" required>
				    </fieldset>
				    <fieldset>
				      <input placeholder="Número de teléfono (opcional)" name="phone" type="tel" tabindex="3">
				    </fieldset>
				    <fieldset>
				      <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">ENVIAR</button>
				    </fieldset>
				       <?php
						if(isset($_POST["submit"])) 
						{
						if(mail($to,$subject,$message,$headers))
						{
						echo "Thanks!!! Your email has been send";
						} else 
						{
						echo "<strong>Failed to send the email</strong>";
						}
						}		
					?>
				  </form>
				  
  		</div>
	</div>
	<footer>
        <div class="container">
            <div class="row">
               <div class="col-md-6 text-center">
					<h5>COMPANÍA</h5>
					<ul>
						<li><i class="fa fa-home" aria-hidden="true"></i><a href="index.php"> 
 Menú Principal</a></li>
						<li><i class="fa fa-user-o" aria-hidden="true"></i>
<a href="sobre/about.html">  Sobre...</a></li>
						<li><i class="fa fa-handshake-o" aria-hidden="true"></i><a href="servicios/servicios.html">  Nuestros Servicios</a></li>
						<li><i class="fa fa-phone" aria-hidden="true"></i><a href="contact.php">  Contactos</a></li>
						<li><i class="fa fa-briefcase" aria-hidden="true"></i><a href="index.php">  Conviertete en un Afiliado (NUEVO)</a></li>
					</ul>
			   </div>
               <div class="col-md-3">
					<h5>¿Necesitas Ayuda?</h5>
					<ul>
						<li><i class="fa fa-user-secret" aria-hidden="true"></i><a href="politica-privacidad/tyc.html">  Política de Privacidad</a></li>
						<li><i class="fa fa-check-square-o" aria-hidden="true"></i><a href="politica-privacidad/tyc.html">  Términos & Condiciones</a></li>
						<li><i class="fa fa-question" aria-hidden="true"></i><a href="faq/faq-es.html">  F.A.Q</a></li>
						<li><i class="fa fa-ban" aria-hidden="true"></i><a href="../cancellation-and-refund.php">  Cancelación</a></li>
					</ul>
			   </div>
            </div>
        </div>
        <div class="white-bg">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<p>© Copyright COMPARE AUTO RENTALS | 2017-2018</p>
					</div>
					<div class="col-md-6 text-right">
						<ul class="footer-social-icon">
							<li><a href="https://web.facebook.com/compareautorentals/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="https://twitter.com/compareautorent/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="https://www.instagram.com/CompareAutoRentals/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>