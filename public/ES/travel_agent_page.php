<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-85516100-1"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());

  		gtag('config', 'UA-85516100-1');
	</script>
	
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Compareautorentals | Official WebSite">
    <meta name="author" content="Compareautorentals | Official WebSite">

    <title>Compare Auto Rentals | Official Website</title>
	    <link rel="shortcut icon" type="image/x-icon" href="../img/favicon.png">
	    <!-- Bootstrap Core CSS -->
	    <link href="../css/bootstrap.min.css" type="text/css" rel="stylesheet">
	
	    <!-- Custom CSS -->
	    <link href="../css/business-casual.css" rel="stylesheet">
	    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
	    <link href="../css/general.css" type="text/css" rel="stylesheet">
	    
	    <!-- Font Awsome -->
	    <link rel="stylesheet" href="../js/font-awesome/css/font-awesome.min.css">
	    
	    <!-- Java Script -->
	    <script src="../js/general-script.js" type="text/javascript"></script>
	    
	    <!-- jQuery library -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>

	    <!-- Latest compiled JavaScript -->
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	    
	    <!-- Fonts -->
	    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
	    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
	    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
	
	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
		<script> var WLConfig_ = { 'step' : 'step1' }; </script>
		
		<!--Start of Zendesk Chat Script-->
		
		
		<script type="text/javascript">
		window.$zopim||(function(d,s){var z=$zopim=function(c){
		z._.push(c)},$=z.s=
		d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
		_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
		$.src='https://v2.zopim.com/?BWafkv1UYCSBn82T47iV9AlS4AfeOAwo';z.t=+new Date;$.
		type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
		</script>
		<script type="text/javascript">
		window.setTimeout(function() {
		$zopim.livechat.button.show();
		$zopim.livechat.window.show();
		$zopim.livechat.bubble.show();
		//You can add more APIs on this line
		}, 10000); //time’s in milliseconds
		</script>
		
		
		<!--End of Zendesk Chat Script-->
		<script> var WLConfig_ = { 'step' : 'step1'}; </script>
</head>

<body class="home">
	<header>
			<div class="container">
				<div class="row">
					<div class="col-md-4 text-center"><a href="index.php"><img src="../img/logo.png" /></a></div>
					<div class="col-md-4 text-center"><h2 style="color:red; "><?php echo "Hola " . $_SESSION['user']; ?></h2></div>
					<div class="col-md-4 text-center"><a href="logout.php" class="btn btn-danger btn-lg active" role="button" aria-pressed="true">Salir</a></div>
				</div>
			</div>
			
	</header>
	<div class="main-slider">
			<div class="row">
			<!-- Navigation -->
				<div class="box">
					<div class="col-lg-12 text-center">
						<div id="carousel-example-generic" class="carousel slide">
						<legend>***Después de realizar una reservación el agente debe contactar a la agencia para realizar una confirmación***</legend>
							<!-- Wrapper for slides -->
							<div class="carousel-inner">
								<div class="item active">
									<img class="img-responsive img-full" src="../img/travel.jpg" alt="" style="height: 100%; width: 100%;border: 2px solid  #e6e6e6;">
									<div class="row search-box">
									  <div class="col-md-4 col-md-offset-4"><div id="_wl-searchbox-holder"></div></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<footer>
			<div class="container text-center">
				<div class="row">
					<div class="col-md-6">
						<h5>Compañía</h5>
						
							<ul>
								<li><i class="fa fa-handshake-o" aria-hidden="true"></i><a href="servicios/servicios.html" target="_blank">  Nuestros Servicios</a></li>
								<li><i class="fa fa-phone" aria-hidden="true"></i><a href="contact.php" target="_blank">  Contactos</a></li>
								<li><i class="fa fa-briefcase" aria-hidden="true"></i><a href="agents_pages/friend.php" target="_blank">  Recomendar a un Amigo (NUEVO)</a></li>
								<li><i class="fa fa-briefcase" aria-hidden="true"></i><a href="agents_pages/banner_link.php" target="_blank">  Compareautorentals Banner de Afiliado (NUEVO)</a></li>
							</ul>
					</div>
	               			<div class="col-md-6">
						<h5>Necesitas Ayuda?</h5>
						
							<ul>
								<li><i class="fa fa-user-secret" aria-hidden="true"> </i><a href="privacy_policy.html" target="_blank">  Política de Privacidad</a></li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"> </i><a href="terms-and-conditions.html" target="_blank">  Términos y Condiciones</a></li>
								<li><i class="fa fa-question" aria-hidden="true"> </i><a href="faq" target="_blank">  
		  F.A.Q</a></li>
								<li><i class="fa fa-ban" aria-hidden="true"> </i><a href="cancellation-and-refund.php" target="_blank">  Cancelación</a></li>
							</ul>
				   	</div>
				</div>
			</div>
	<div class="white-bg">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<p>© Copyright COMPARE AUTO RENTALS | 2017</p>
				</div>
				<div class="col-md-6 text-right">
					<ul class="footer-social-icon">
						<li><a href="https://web.facebook.com/compareautorentals/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="https://twitter.com/compareautorent" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="https://www.instagram.com/CompareAutoRentals/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>			
<script>
		var WLConfig_ = WLConfig_ || {};
		WLConfig_.user = "CAR_support";
		WLConfig_.source = WLConfig_.user;
		WLConfig_.lang = "es_ES";
		WLConfig_.api = 'https://gfa.carrentalgateway.com';
		WLConfig_.proxy = '/proxy.php';
		(function(T, S, G, a, t, e, w, a, y) {
			var url = S.api + '/wl.js?v=1&u=' + S.user;
			var f = T.getElementsByTagName('script')[0];
			var j = T.createElement('script');
			j.async = true;
			j.src = url;
			f.parentNode.insertBefore(j, f);
		})(document, WLConfig_);
	</script>
	</footer>
	</body>
</html>