<?php
define('API_URL', 'https://gfa.carrentalgateway.com');
define('USER', 'CAR_support');
define('API_KEY', '25615800d45c121437fae12a96b75e42');
?>
<?php
function getRemoteIp() {
	if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && strlen($_SERVER['HTTP_X_FORWARDED_FOR']) > 0) {
		$ip = array_map('trim', explode(',', (string)$_SERVER['HTTP_X_FORWARDED_FOR']));
	} else {
		if (isset($_SERVER['REMOTE_ADDR'])) {
			$ip = array_map('trim', explode(',', (string)$_SERVER['REMOTE_ADDR']));
		}
	}
	return isset($ip[0]) ? $ip[0] : $ip;
}

function getTrackingData() {
	return array(
		'browser' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '',
		'ip' => getRemoteIp(),
		'source' => isset($_COOKIE['tf_source']) ? $_COOKIE['tf_source'] : '',
		'session_id' => ''
	);
}

header('Content-type: application/json;charset=utf-8');

$data = json_decode(file_get_contents('php://input'), true);

$ch = curl_init();

$params = $_GET;
$params['user'] = USER;
$params['api_key'] = API_KEY;

$data['tracking_data'] = getTrackingData();

curl_setopt($ch, CURLOPT_URL, API_URL . '/api/?' . http_build_query($params));
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
curl_setopt($ch, CURLOPT_TIMEOUT, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	"HTTP_X_FORWARDED_FOR: " . getRemoteIp()
));

echo curl_exec($ch);
