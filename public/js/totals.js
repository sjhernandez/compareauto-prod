var currencies, defaultcurrency, currentxrate, currentcurrency;

$(document).ready(function() {
    let pnow, ploc, ptf, ptotal, paynow1;

    $('#cdwckbox').prop('checked', false)
    $('#nocdw').hide();
    $('#cdwckbox').click(function() {
        $('#nocdw').toggle();
    });
    $('#residence-box').hide();
    $('#rescheckbox').click(function() {
        $('#residence-box').toggle();
    });
    var xrateelm = document.getElementById('xrate')
    xrate1 = Number(xrateelm.dataset.xrate)

    defaultcurrency = $('#currency-total-left').attr('data-currency');
    $(".jstotal select").val(defaultcurrency);



    $.getJSON('https://compareauto.herokuapp.com/fullcurrency', function(data) {
        currencies = data;
        ccurrent = currencies.find(function(element) {
            return element.curr == defaultcurrency
        });
    });
    pnow0 = parseFloat($('#paynowo').text());
    ploc0 = parseFloat($('#payloco').text());
    ptf0 = parseFloat($('#ptfo').text());
    ptotal0 = parseFloat($('#ptotalo').text());
    pnowbase = (pnow0 / xrate1).toLocaleString({ minimumFractionDigits: 0, maximumFractionDigits: 2 }, { style: 'currency', currency: defaultcurrency })
    plocbase = (ploc0 / xrate1).toLocaleString({ minimumFractionDigits: 2, maximumFractionDigits: 2 }, { style: 'currency', currency: defaultcurrency })
    ptotalbase = (ptotal0 / xrate1).toLocaleString({ minimumFractionDigits: 2, maximumFractionDigits: 2 }, { style: 'currency', currency: defaultcurrency })



    pnow = parseFloat($('#paynow').text(pnowbase));
    ploc = parseFloat($('#payloc').text(plocbase));
    ptf = parseFloat($('#ptf').text(ptotalbase));
    ptotal = parseFloat($('#ptotal').text(ptotalbase));
    pnow1 = parseFloat($('#paynow1').text(pnowbase));
    ploc1 = parseFloat($('#payloc1').text(plocbase));
    ptf1 = parseFloat($('#ptf1').text());
    ptotal1 = parseFloat($('#ptotal1').text(ptotalbase));
    $('#adv-cost').text(ptotalbase)



});
// this is the total calculation for booking page

window.onload = () => {
    const originalcurrency = $('.jstotal select').val()


    pnow10 = (pnow0 / xrate1).toFixed(2)
    ploc10 = (ploc0 / xrate1).toFixed(2)
    ptf1 = parseFloat($('#ptfo').text());
    ptotal10 = (ptotal0 / xrate1).toFixed(2)


    const totalcal = () => {



        $('.jstotal select').on('change', function() {
            currentcurrency = $('option:selected', this).text();
            let crs = currencies.find(function(element) {
                return element.curr == currentcurrency;
            });
            pnow10 = (pnow0 * crs.rate).toFixed(2)
            ploc10 = (ploc0 * crs.rate).toFixed(2)
            ptf1 = parseFloat($('#ptfo').text());
            ptotal10 = (ptotal0 * crs.rate).toFixed(2)
            getTotal(pnow10, ptotal10, currentcurrency)

            $('#extraslocal select').each(function() {
                let oldcost = $(this).attr('data-cost')
                let newcost = (oldcost * crs.rate).toFixed(2)
                $(this).attr('data-costx', newcost)
                $(this).parent().parent().children('.extra-cost').text(currentcurrency + " " + newcost);
                let qty = this.value;

                let total = (qty * newcost).toFixed(2);
                $(this).attr('data-selecttotal', total);


            })
            let oldcost1 = $('#SProtector').attr('data-cost');
            let newcost1 = (oldcost1 * crs.rate).toFixed(2)
            $('#SProtector').attr('data-costx', newcost1);
            $('#SProtectorcost').text(currentcurrency + " " + newcost1)





            let oldcost2 = $('#CDW').attr('data-cost');
            let newcost2 = (oldcost2 * crs.rate).toFixed(2)
            $('#CDW').attr('data-costx', newcost2);
            $('#CDWcost').text(currentcurrency + " " + newcost2)


            pnow10 = (pnow0 * crs.rate).toFixed(2)
            ploc10 = (ploc0 * crs.rate).toFixed(2)
            ptf1 = parseFloat($('#ptfo').text());
            ptotal10 = (ptotal0 * crs.rate).toFixed(2)
            getTotal(pnow10, ptotal10, currentcurrency)
        })

        console.log(currentcurrency)



        pnow = parseFloat($('#paynow').text());
        ploc = parseFloat($('#payloc').text());
        ptf = parseFloat($('#ptf').text());
        ptotal = parseFloat($('#ptotal').text());
        pnow1 = parseFloat($('#paynow1').text());
        ploc1 = parseFloat($('#payloc1').text());
        ptf1 = parseFloat($('#ptf1').text());
        ptotal1 = parseFloat($('#ptotal1').text());
        // insurance variables

        // pnow = document.getElementById('paynow').innerText;
        let defaultcurrency = $(".jstotal select").val();
        // total box items

        //  this updates the data values on the select dropdowns

        $('#extraslocal select').on('change', function() {


            let qty = this.value;
            let cost6 = parseFloat($(this).attr('data-costx'));
            let total = (qty * cost6).toFixed(2);
            $(this).attr('data-selecttotal', total);
            currentcurrency = currentcurrency || defaultcurrency;

            getTotal(pnow10, ptotal10, currentcurrency);
            // var a = $('#mydiv').data('myval'); //getter

            // $('#mydiv').data('myval',20); //setter
        });
        $('#SProtector').on('change', function() {
            let costsp = parseFloat($(this).attr('data-costx'));
            if ($(this).prop("checked")) {
                $(this).attr('data-total', costsp);
            } else {
                $(this).attr('data-total', '0');
            }
            currentcurrency = currentcurrency || defaultcurrency;
            getTotal(pnow10, ptotal10, currentcurrency);


        });
        $('#CDW').on('change', function() {
            let costcdw = parseFloat($(this).attr('data-costx'));
            if ($(this).prop("checked")) {
                $(this).attr('data-total', costcdw);
            } else {
                $(this).attr('data-total', '0');
            }
            currentcurrency = currentcurrency || defaultcurrency;

            getTotal(pnow10, ptotal10, currentcurrency);



        })


        const getTotal = (pn10, pt10, currency) => {
            let payTotal;
            let insTotal = 0;
            let payLocal = 0;
            let paynow = 0;
            let selTotal = 0;
            let ptfa = parseFloat($('#ptf').text());
            $('select').each(function() {

                if (!isNaN($(this).attr('data-selecttotal'))) {
                    let indcost = $(this).attr('data-selecttotal')
                    selTotal += Number(indcost);
                }

            });

            let sp = $('#SProtector').attr('data-total');
            let cdw = $('#CDW').attr('data-total');
            insTotal = (Number(sp) + Number(cdw)).toFixed(2);
            paynow9 = (Number(pn10) + Number(insTotal)).toFixed(2)
            paynow = (Number(pn10) + Number(insTotal)).toLocaleString({ minimumFractionDigits: 2, maximumFractionDigits: 2 }, { style: 'currency', currency: currency })
            payTotal9 = (Number(pt10) + Number(insTotal) + Number(selTotal)).toFixed(2)
            payLocal = (Number(payTotal9) - Number(paynow9)).toLocaleString({ minimumFractionDigits: 2, maximumFractionDigits: 2 }, { style: 'currency', currency: currency })
            payTotal = (Number(pt10) + Number(insTotal) + Number(selTotal)).toLocaleString({ minimumFractionDigits: 2, maximumFractionDigits: 2 }, { style: 'currency', currency: currency })


            pnow = $('#paynow').text(paynow);
            ploc = $('#payloc').text(payLocal);

            ptotal = $('#ptotal').text(payTotal);
            pnow1 = $('#paynow1').text(paynow);
            ploc1 = $('#payloc1').text(payLocal);
            $('#adv-cost').text(payTotal)
            ptotal1 = $('#ptotal1').text(payTotal)
            insTotal = 0;
            payLocal = 0;
            payLocal9 = 0;
            paynow = 0;
            paynow9 = 0;
            selTotal = 0;
            payTotal9 = 0;
        };



    };

    totalcal();

    // function checktotalscorrect(item) {

    //     let cost1 = parseFloat($(this).data('costx'));
    //     if ($(this).prop("checked")) {
    //         $(this).attr('data-total', cost1);
    //     } else {
    //         $(this).attr('data-total', '0');
    //     }

    // }

};