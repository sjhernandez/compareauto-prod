$(document).ready(function() {
        //get geolocation information from client
        $.getJSON('https://compareauto.herokuapp.com/users', function(data) {
            var countryName = data.country_name;
            var countryCode1 = data.country_code.toString();
            var countryCode = countryCode1.toLowerCase();

            var currencyCode = data.currency.code;
            var currencySymbol = data.currency.symbol;
            $('#currency-form').html(currencyCode);
            $('#country-form').html(countryName);
            $('#currency').val(currencyCode)
            $('#countries').val(countryCode)


        });


        //this is the function that gets and displays the pick up locations on the search form. 
        $('#location').keyup(function() {
            let query1 = $(this).val();
            if (query1 != '' && query1.length > 2) {
                $.ajax({
                    url: " https://compareauto.herokuapp.com/locations",
                    method: "POST",
                    cache: false,
                    headers: { "cache-control": "no-cache" },
                    data: {
                        query: query1
                    },
                    success: function(data) {

                        $('#countryList').fadeIn("fast");
                        $('#countryList').html(data);


                    }

                });


            }

        });
        $('#terms').click(function() {
            if (this.checked) {
                $('#reserve-my').prop('disabled', false);
            } else {
                $('#reserve-my').prop('disabled', true);
            }

        });
        // end function pick up locations
        $('#searchform').submit(function() {

            $('.form-input').hide();
            $('.main').removeClass('main jumbotron');
            $('.moving-car').css('display', 'block');

        });
        $('#searchform2').submit(function() {

            $('#resultsbody').hide();

            $('.moving-car').css('display', 'block');

        });




        let $select = $('#countries');
        // });

        $.getJSON('https://compareauto.herokuapp.com/json', function(data) {

            let currents = data['Country']

            for (const current of currents) {
                if (current["Iso"] == "us") {
                    let options = '<option value="' + current["Iso"] + '"selected>' + current["Name"] + '</option>'
                    $select.append(options);
                    // console.log(options);
                } else {

                    let options = '<option value="' + current["Iso"] + '">' + current["Name"] + '</option>'
                    $select.append(options);


                }

            }

        });

        //end func country list dropdown. 

        // residence checkbox toggle function. 
        //    $('#residence-box').hide(); 
        $('#rescheckbox').click(function() {
            $('#residence-box').toggle();
        });

        // end residenc checkbox function



        //start drop off location search on form -- not working filling up the pulocation with the info
        $('#drop-off-location').keyup(function() {
            let query = $(this).val();

            if (query != '' && query.length > 2) {
                $.ajax({
                    url: " https://compareauto.herokuapp.com/locations",
                    method: "POST",


                    data: {
                        query: query
                    },
                    success: function(data) {

                        $('#countryList1').fadeIn("fast");
                        $('#countryList1').html(data);


                    }

                });


            }

        });
        // clears the country list on click on search form pu locations
        $(document).on('click', ' #countryList li', function() {

            $('#location').val($(this).find('p:first').text());
            $('#locnumber').attr('value', $(this).find('p:last').text());
            $('#countryList').fadeOut("fast");


        });
        // clears the country list on click on search form drop off locations
        $(document).on('click', '#countryList1 li', function() {

            $('#drop-off-location').val($(this).find('p:first').text());
            $('#locnumber1').attr('value', $(this).find('p:last').text());
            $('#countryList1').fadeOut("fast");


        });
        //toggles the check box for different location dropoff

        $('#dropcheckbox').click(function() {
            $('#drop-off-location').toggle();


        });



    })
    //end of document ready section

const sortAsc = () => {

        let $wrapper = $('.car-holder');


        $wrapper.find('.result').sort(function(a, b) {

            return b.getAttribute('data-rating') - a.getAttribute('data-rating')

        }).appendTo($wrapper);



    }
    // console.log($res)





const sortDesc = () => {

    let $wrapper = $('.car-holder');


    $wrapper.find('.result').sort(function(a, b) {

        return a.getAttribute('data-rating') - b.getAttribute('data-rating')

    }).appendTo($wrapper);





}






// ends booking total calculation


$(function() {
    function log(message) {
        $("<div>").text(message).prependTo("#log");
        $("#log").scrollTop(0);
    }


});

//date picker configurations 
$(function() {
    $("#datepicker").datepicker({
        numberOfMonths: 1,
        showButtonPanel: true,
        minDate: 0,
        showOn: "both",
        buttonImage: "./img/calendar.svg",
        buttonImageOnly: true,

        defaultDate: +3,
        onSelect: function(dateText, inst) {
            $("#datepicker1").datepicker("option", "minDate", $("#datepicker").datepicker("getDate"));
        }

    });
});

$(function() {
    $("#datepicker1").datepicker({
        numberOfMonths: 1,
        showButtonPanel: true,
        minDate: 0,
        showOn: "both",
        buttonImage: "./img/calendar.svg",
        buttonImageOnly: true,

        defaultDate: +7,

    });
});
//datepicker ends

// $(document).ready(function() {

//     const cksizeprice = () => {
//         const items = Array.from(document.querySelectorAll('.d-price'))
//         const changeFont = () => {

//             $('.d-price').css('font-size', '30px');
//             $('.price-day').css('font-size', '15px');
//             $('.day-display').css('font-size', '15px');



//         }
//         for (const item of items) {
//             number = item.dataset.price
//             number1 = Number(number)
//             console.log(number)
//             if (number1 > 999) {
//                 changeFont();
//                 break;
//             }
//         }



//     }
//     cksizeprice();
// })


$(document).ready(function() {
    // var $results=$('.result').html(function(){
    //     /* swap text with catgories to see results better */

    // })


    var $checks = $(':checkbox[name^=fil]').change(function() {
        var $checked = $checks.filter(':checked');
        let $results = $('.result');

        /* show all when nothing checked*/
        if (!$checked.length) {
            $results.show();
            return;
        }
        /* create array of checked values */
        var checkedCompany = $.map($checked, function(el) {
            if (el.dataset.company == "supplier") {
                return el.value
            }

        })
        var checkLoc = $.map($checked, function(el) {

            if (el.dataset.location == "location") {
                return el.value
            }


        });

        var checkTrans = $.map($checked, function(el) {

            if (el.dataset.transmission == "transmission") {
                return el.value
            }
        });
        var checkedsize = $.map($checked, function(el) {

            if (el.dataset.size == "size") {
                return el.value
            }


        });

        const filter = (rest) => {
            $results.hide()
            if (checkedCompany.length > 0) {
                rest = rest.filter(function() {
                    let company = $(this).data('company')
                    return checkedCompany.includes(company);
                })
            }
            if (checkedsize.length > 0) {
                rest = rest.filter(function() {
                    let size = $(this).data('size');
                    return checkedsize.includes(size)


                })
            }
            if (checkLoc.length > 0) {
                rest = rest.filter(function() {
                    let local = $(this).data('location');

                    return checkLoc.includes(local);

                })
            }
            if (checkTrans.length > 0) {
                rest = rest.filter(function() {
                    let trans = $(this).data('transmission');

                    return checkTrans.includes(trans);

                })
            }

            rest.map(function() {
                $(this).show();
            })

        }
        filter($results)




    });
});