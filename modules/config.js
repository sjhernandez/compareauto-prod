module.exports = {

    booking: 'https://api.carrentalgateway.com/xml/bookings',
    quoteurl: 'https://api.carrentalgateway.com/xml',
    localhost: 'https://compareauto.herokuapp.com/',
    administration: '<RQ><Administration><Username>compareauto_website</Username><Password>pWq212/2_</Password><Agent>CAR_support</Agent></Administration>',
    testdata: '<RQ><Administration><Username>compareauto_website</Username><Password>pWq212/2_</Password><Agent>CAR_support</Agent></Administration><Requests><VehicleRQ lang="33"pickUpDateTime="2018-07-19T12:00:00"dropOffDateTime="2018-07-29T12:00:00"><PickUpLocation code="574"codeContext="INTERNAL_LOCATION_ID" /><DropOffLocation code="574"codeContext="INTERNAL_LOCATION_ID" />  <Driver age="30" /><ResidenceCountry code="gb" /></Requests></RQ>',
    currency: '<RQ><Administration><Username>compareauto_website</Username><Password>pWq212/2_</Password><Agent>CAR_support</Agent></Administration><Requests><CurrencyRQ base="USD" /></Requests></RQ>'
};