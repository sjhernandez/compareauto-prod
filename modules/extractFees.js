module.exports = (data, xrate) => {
    // console.log(data)
    if (!data.Vehicle.Fees) {

        return "Taxes and fees included in price"
    }
    let feesInfo = data.Vehicle.Fees.Fee;
    let feesData = {};
    let feeArray = [];
    let copyFeeData = {};
    if (Array.isArray(feesInfo)) {

        for (const fee of feesInfo) {
            feesData.otacode = fee._otaCode;
            feesData.name = fee._name;
            feesData.cost = fee.Price._amount;
            feesData.currency = fee.Price._currency;
        }
    } else {
        feesData.otacode = feesInfo._otaCode;
        feesData.name = feesInfo._name;
        feesData.cost = feesInfo.Price._amount;
        feesData.currency = feesInfo.Price._currency;


    }
    copyFeeData = Object.assign({}, feesData);
    feeArray.push(copyFeeData);


    return feeArray;
}