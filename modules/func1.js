module.exports ={ 
 sortByPrice: (carentals) => { 
    carRentals.sort(function(obj1, obj2){
      return obj1.price - obj2.price;
  
  });
  return carentals;
  },



  processObj: (jsonObject)=>{
    var combinedVehicles = [];
    var availableBranches = jsonObject.VehicleRS.AvailableBranches.AvailableBranch;
    var branchLocation = jsonObject.VehicleRS.PickUpLocation._locationName;
    var carsInBranches = new Object();
    // carsInBranches.branchName = jsonObject.VehicleRS.PickUpLocation._locationName
    function processBranch(branch) {

      carsInBranches.branchLogo = (branch.PickUpBranch.Supplier.Logo._logo) ? branch.PickUpBranch.Supplier.Logo._logo : branch.PickUpBranch.Supplier.Logo._logoSvg;
      carsInBranches.branchName = branch.PickUpBranch.Supplier._name;
      carsInBranches.dropoffInfo = branch.DropOffBranch;
      carsInBranches.pickupInfo = branch.PickUpBranch;
      carsInBranches.pickupLoc = branch.PickUpBranch._vehicleAtName;
      var cars = branch.Vehicles.Vehicle;

      if (Array.isArray(cars)) {
        for (let car of cars) {
          carsInBranches.packages = car.Packages
          carsInBranches.vid = car.VehicleInfo._id
          carsInBranches.transmission = car.VehicleInfo._transmission
          carsInBranches.airco = car.VehicleInfo._airco
          carsInBranches.name = car.VehicleInfo._name
          carsInBranches.size = car.VehicleInfo._subGroup
          carsInBranches.doors = car.VehicleInfo._doors
          carsInBranches.seats = car.VehicleInfo._seats
          carsInBranches.bigSuitCases = car.VehicleInfo._bigSuitcases
          carsInBranches.smallSuitCases = car.VehicleInfo._smallSuitcases
          carsInBranches.carUrl = car.VehicleInfo.Image._url
          carsInBranches.branchLocation = branchLocation
          carsInBranches.rateRef = ''
          if (Array.isArray(car.Packages.Package)) {
            carsInBranches.price = car.Packages.Package[0].VehiclePrice.SellingPrice._amount;
            carsInBranches.price1 = car.Packages.Package[0].VehiclePrice.SellingPrice._estimatedTotalAmount
            carsInBranches.rentalDays = car.Packages.Package[0].VehiclePrice._rentalDays;
            carsInBranches.costPerDay = Math.floor(carsInBranches.price / carsInBranches.rentalDays);
            // carsInBranches.mileage = car.Packages.Package[0].Inclusions.Inclusion[0]._name ;
            // carsInBranches.incOne = car.Packages.Package[0].Inclusions.Inclusion[1]._name;
            //  if (car.Packages.Package[0].Inclusions.Inclusion[2]._name){ carsInBranches.incTwo = car.Packages.Package[0].Inclusions.Inclusion[2]._name};
            carsInBranches.rateRef = car.Packages.Package[0].VehiclePrice.RateReference;
          } else {

            carsInBranches.price = car.Packages.Package.VehiclePrice.SellingPrice._amount;
            carsInBranches.price1 = car.Packages.Package.VehiclePrice.SellingPrice._estimatedTotalAmount
            carsInBranches.rentalDays = car.Packages.Package.VehiclePrice._rentalDays;
            carsInBranches.costPerDay = Math.floor(carsInBranches.price / carsInBranches.rentalDays);
            // carsInBranches.mileage = car.Packages.Package[0].Inclusions.Inclusion[0]._name;
            // carsInBranches.incOne = car.Packages.Package.Inclusions.Inclusion[1]._name;
            // if (car.Packages.Package.Inclusions.Inclusion[2]._name){ carsInBranches.incTwo =   car.Packages.Package.Inclusions.Inclusion[2]._name };
            carsInBranches.rateRef = car.Packages.Package.VehiclePrice.RateReference;


          }

          function cloneObject(obj) {
            var clone = {};
            for (var i in obj) {
              if (obj[i] != null && typeof (obj[i]) == "object")
                clone[i] = cloneObject(obj[i]);
              else
                clone[i] = obj[i];
            }
            return clone;
          }
          // var copyCars = Object.assign({}, carsInBranches)
          var copyCars = cloneObject(carsInBranches)
          combinedVehicles.push(copyCars);


        }

      }




    }
    if (Array.isArray(availableBranches)) {
      for (let branch of availableBranches) {

        processBranch(branch);

      }
    } else {
      processBranch(availableBranches);
    }
    return combinedVehicles
  }
}

