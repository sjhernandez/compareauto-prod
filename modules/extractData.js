let extractTerms = require('./extractTerms');
let extractTermService = require('./extractTermService')
let dateFormat = require('dateformat');
module.exports = (carData, xrate, currency) => {
    let rentalCar = {};
    let formatDate = (datetoformat) => {
        date = new Date(datetoformat);
        return dateFormat(date, "dddd, mmmm dS, yyyy, h:MM:ss TT");

    }
    let monthFormat = (datetoformat) => {
        date = new Date(datetoformat);
        return dateFormat(date, "mmmm");
    }
    rentalCar.pickupDate = formatDate(carData._pickUpDateTime);
    rentalCar.puMonth = monthFormat(carData._pickUpDateTime);
    rentalCar.dropoffDate = formatDate(carData._dropOffDateTime);
    rentalCar.customerAge = carData.Customer._age;
    rentalCar.resCountry = carData.Customer._residenceCountry
    rentalCar.resCode = carData.Customer._residenceCountryCode;
    rentalCar.puLocBranch = carData.PickUpLocation._locationName;
    rentalCar.puLat = carData.PickUpBranch._latitude;
    rentalCar.puLon = carData.PickUpBranch._longitude;
    rentalCar.puInstructions = carData.PickUpBranch.SpecialInstructions;
    rentalCar.puAddress = carData.PickUpBranch._address;
    rentalCar.puPhone = carData.PickUpBranch._phoneNumber;
    rentalCar.reqFlight = carData.PickUpBranch._requireFlighNo;
    rentalCar.carAt = carData.PickUpBranch._vehicleAtName;
    rentalCar.carCompany = carData.PickUpBranch.Supplier._name;
    rentalCar.ccLogo = carData.PickUpBranch.Supplier.Logo._logo;
    rentalCar.doLocBranch = carData.DropOffLocation._locationName;
    rentalCar.doLat = carData.DropOffBranch._latitude;
    rentalCar.doLon = carData.DropOffBranch._longitude;
    rentalCar.doInstructions = carData.DropOffBranch.SpecialInstructions;
    rentalCar.doAddress = carData.DropOffBranch._address;
    rentalCar.doPhone = carData.DropOffBranch._phoneNumber;


    rentalCar.image = carData.Vehicle.VehicleInfo.Image._url || 'img/search-placeholder.png';
    rentalCar.air = carData.Vehicle.VehicleInfo._airco;
    rentalCar.bigSuitcases = carData.Vehicle.VehicleInfo_bigSuitcases;
    rentalCar.doors = carData.Vehicle.VehicleInfo._doors;
    rentalCar.type = carData.Vehicle.VehicleInfo._group; // weather its a car or truck
    rentalCar.carId = carData.Vehicle.VehicleInfo._id; // number for car
    rentalCar.name = carData.Vehicle.VehicleInfo._name;
    rentalCar.seats = carData.Vehicle.VehicleInfo._seats;
    rentalCar.smallSuitcases = carData.Vehicle.VehicleInfo._smallSuitcases;
    rentalCar.class = carData.Vehicle.VehicleInfo._subGroup; //car class
    rentalCar.supplierCode = carData.Vehicle.VehicleInfo._supplierCode; // four letter iata code
    rentalCar.transmission = carData.Vehicle.VehicleInfo._transmission;
    rentalCar.rentalDays = carData.Vehicle.VehiclePrice._rentalDays;
    rentalCar.paymentCurrency = carData.Vehicle.VehiclePrice._paymentCurrency;
    rentalCar.rateRef = carData.Vehicle.VehiclePrice.RateReference;
    rentalCar.total = carData.Vehicle.VehiclePrice.SellingPrice._amount;
    rentalCar.payNow = carData.Vehicle.VehiclePrice.SellingPrice._profit;
    rentalCar.payLater = (carData.Vehicle.VehiclePrice.SellingPrice._estimatedTotalAmount - carData.Vehicle.VehiclePrice.SellingPrice._profit).toFixed(2);
    rentalCar.estTotal = carData.Vehicle.VehiclePrice.SellingPrice._estimatedTotalAmount;
    rentalCar.estTotalDisplay = carData.Vehicle.VehiclePrice.SellingPrice._estimatedTotalAmount.toLocaleString({ minimumFractionDigits: 2, maximumFractionDigits: 2 }, { style: 'currency', currency: currency });;
    rentalCar.chargedCurrency = carData.Vehicle.VehiclePrice.SellingPrice.ExchangeRates._currency;
    rentalCar.exchangeRate = carData.Vehicle.VehiclePrice.SellingPrice.ExchangeRates.ExchangeRate._exchangeRate
    rentalCar.exchangeRateFrom = carData.Vehicle.VehiclePrice.SellingPrice.ExchangeRates.ExchangeRate._from
    rentalCar.exchangeRateTo = carData.Vehicle.VehiclePrice.SellingPrice.ExchangeRates.ExchangeRate._to
    rentalCar.localCurrency = carData.Vehicle.VehiclePrice.SellingPrice.ExchangeRates._localCurrency;
    rentalCar.feesTotal = (rentalCar.estTotal - rentalCar.total).toFixed(2);
    rentalCar.termconditions = extractTerms(carData.Terms.SubSection[0].Paragraph)
    rentalCar.termservice = extractTermService(carData.Terms)
    return rentalCar;
}