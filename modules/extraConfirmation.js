const extractExtras = require('../modules/extractExtras');
const extractFees = require('../modules/extractFees');
const extractTerms = require('../modules/extractTerms');
let dateFormat = require('dateformat');

module.exports = (details) => {

    let confDet = {};
    let detailsArray = [];
    let formatDate = (datetoformat) => {
        date = new Date(datetoformat);
        return dateFormat(date, "dddd, mmmm dS, yyyy, h:MM:ss TT");

    }

    let booking = details.RS.Responses.BookRS;
    confDet.resNumber = booking._reservNum;
    confDet.puDate = formatDate(booking._pickUpDateTime);
    confDet.doDate = formatDate(booking._dropOffDateTime);
    confDet.supplierReference = booking._supplierReference;
    confDet.pulocation = booking.PickUpLocation._locationName;
    confDet.dolocation = booking.DropOffLocation._locationName;
    confDet.pulong = booking.PickUpBranch._longitude;
    confDet.pulat = booking.PickUpBranch._latitude;
    confDet.openHours = booking.PickUpBranch._pickUpDayOpenHours;
    confDet.carAt = booking.PickUpBranch._vehicleAtName;
    confDet.supplierLogo = booking.PickUpBranch.Supplier.Logo._logo;
    confDet.supplierName = booking.PickUpBranch.Supplier._name;
    confDet.puInstructions = booking.PickUpBranch.SpecialInstructions;
    confDet.doInstructions = booking.DropOffBranch.SpecialInstructions;
    confDet.customerName = booking.Customer._firstName + ' ' + booking.Customer._lastName;
    confDet.customerTitle = booking.Customer._title;
    confDet.customerEmail = booking.Customer.Contact._email;
    confDet.customerPhone = booking.Customer.Contact._phone;
    confDet.paymentCurrency = booking.Vehicle.VehiclePrice._paymentCurrency;
    confDet.localCurrency = booking.Vehicle.VehiclePrice._localCurrency;
    // confDet.extras = extractExtras(booking);
    // confDet.fees = extractFees(booking);
    confDet.carImage = booking.Vehicle.VehicleInfo.Image._url;
    confDet.carClass = booking.Vehicle.VehicleInfo._subGroup;
    confDet.carName = booking.Vehicle.VehicleInfo._name
    confDet.rentalDays = booking.Vehicle.VehiclePrice._rentalDays;
    confDet.total = booking.Vehicle.VehiclePrice.SellingPrice._estimatedTotalAmount;
    confDet.payNow = Number(booking._paidAmount).toLocaleString({ minimumFractionDigits: 2, maximumFractionDigits: 2 }, { style: 'currency', currency: 'USD' });
    confDet.taxes = (booking.Vehicle.VehiclePrice.SellingPrice._estimatedTotalAmount - booking.Vehicle.VehiclePrice.SellingPrice._amount).toFixed(2);
    confDet.payAtPu = (booking.Vehicle.VehiclePrice.SellingPrice._estimatedTotalAmount - booking.Vehicle.VehiclePrice.SellingPrice._profit).toLocaleString({ minimumFractionDigits: 2, maximumFractionDigits: 2 }, { style: 'currency', currency: confDet.localCurrency })
    confDet.price = booking.Vehicle.VehiclePrice.SellingPrice._amount;
    confDet.dailyPrice = Math.floor(confDet.price / confDet.rentalDays);
    confDet.terms = extractTerms(booking.Terms.SubSection[0].Paragraph)
    confDet.xrate = booking.Vehicle.VehiclePrice.SellingPrice.ExchangeRates.ExchangeRate._exchangeRate
    confDet.xratecurr = booking.Vehicle.VehiclePrice.SellingPrice.ExchangeRates.ExchangeRate._from
    confDet.locPay1 = Number(booking.Vehicle.VehiclePrice.SellingPrice.AvailablePaymentTypes.PaymentType.Payment[1]._amount).toLocaleString({ minimumFractionDigits: 2, maximumFractionDigits: 2 }, { style: 'currency', currency: confDet.localCurrency })
    confDet.locPay = booking.Vehicle.VehiclePrice.SellingPrice.AvailablePaymentTypes.PaymentType.Payment[1]._amount
    confDet.paid = booking.Vehicle.VehiclePrice.SellingPrice.AvailablePaymentTypes.PaymentType.Payment["0"]._amount
    confDet.locpayusd = Number(confDet.locPay * confDet.xrate).toLocaleString({ minimumFractionDigits: 2, maximumFractionDigits: 2 }, { style: 'currency', currency: 'USD' });
    confDet.totalUSD = Number(booking.Vehicle.VehiclePrice.SellingPrice._amount).toLocaleString({ minimumFractionDigits: 2, maximumFractionDigits: 2 }, { style: 'currency', currency: 'USD' });


    return confDet;
}