module.exports = (terms) => {
    let page = ''
    let locdep = terms.SubSection[1].Paragraph.ListItem;
    let cdw = terms.SubSection[2].Paragraph.ListItem;
    let fuelchg = terms.SubSection[3].Paragraph.ListItem;
    let locdep_title = terms.SubSection[1]._SubTitle;
    let cdw_title = terms.SubSection[2]._SubTitle;
    let fuelchg_title = terms.SubSection[3]._SubTitle;
    page += `<h2>${locdep_title}</h2>`
    for (const item of locdep) {
        let html1 = item['#cdata-section']
        page += `<li>${html1}</li>`;

    }
    page += `<h2>${cdw_title}</h2>`
    for (const item1 of cdw) {
        let html1 = item1['#cdata-section']
        page += `<li>${html1}</li>`;

    }
    page += `<h2>${fuelchg_title}</h2>`

    for (const item2 of fuelchg) {
        let html1 = item2['#cdata-section']
        page += `<li>${html1}</li>`;

    }

    // console.log(page)

    return page


    // carData.Terms.SubSection[1].Paragraph.ListItem
    // carData.Terms.SubSection[1]._SubTitle
    // carData.Terms.SubSection[1].Paragraph.ListItem[0]["#cdata-section"]
}