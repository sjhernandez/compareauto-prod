let extractRating = require('./supplierRating');
module.exports = (jsonObject, xrate, currency) => {
    let combinedVehicles = [];
    let xrate1 = xrate

    let availableBranches = jsonObject.VehicleRS.AvailableBranches.AvailableBranch;
    let branchLocation = jsonObject.VehicleRS.PickUpLocation._locationName;
    let dropOffLocation = jsonObject.VehicleRS.DropOffLocation._locationName;
    let carsInBranches = new Object();


    // carsInBranches.branchName = jsonObject.VehicleRS.PickUpLocation._locationName
    function processBranch(branch) {

        carsInBranches.branchLogo = branch.PickUpBranch.Supplier.Logo._logo || branch.PickUpBranch.Supplier.Logo._logoSvg;
        carsInBranches.branchName = branch.PickUpBranch.Supplier._name;
        carsInBranches.supRating = Number(extractRating(carsInBranches.branchName))
        carsInBranches.supPercent = Math.floor((carsInBranches.supRating / 5 * 100) / 10) * 10;
        carsInBranches.dropoffInfo = branch.DropOffBranch;
        carsInBranches.pickupInfo = branch.PickUpBranch;
        carsInBranches.locationId = branch.PickUpBranch._locationId;
        carsInBranches.pickupLoc = branch.PickUpBranch._vehicleAtName;
        carsInBranches.localCurrency = branch.PickUpBranch._localCurrency

        let cars = branch.Vehicles.Vehicle;

        if (Array.isArray(cars)) {
            for (let car of cars) {
                carsInBranches.Packages = car.Packages.Package;
                carsInBranches.vid = car.VehicleInfo._id;
                carsInBranches.transmission = car.VehicleInfo._transmission;
                carsInBranches.airco = car.VehicleInfo._airco;
                carsInBranches.name = car.VehicleInfo._name;
                carsInBranches.size = car.VehicleInfo._subGroup;
                carsInBranches.doors = car.VehicleInfo._doors;
                carsInBranches.seats = car.VehicleInfo._seats;
                carsInBranches.bigSuitCases = car.VehicleInfo._bigSuitcases;
                carsInBranches.smallSuitCases = car.VehicleInfo._smallSuitcases;
                carsInBranches.carUrl = car.VehicleInfo.Image._url || 'img/search-placeholder.png';
                carsInBranches.branchLocation = branchLocation;
                carsInBranches.dropLocation = dropOffLocation;
                carsInBranches.rateRef = '';
                // console.log(car.Packages.Package.VehiclePrice)
                if (car.Packages.Package.VehiclePrice) {
                    carsInBranches.exchangerate = car.Packages.Package.VehiclePrice.SellingPrice.ExchangeRates.ExchangeRate || car.Packages.Package[0].VehiclePrice.SellingPrice.ExchangeRates.ExchangeRate || car.Packages.Package.VehiclePrice.VehiclePrice.SellingPrice.ExchangeRates.ExchangeRate;
                    carsInBranches.price = ((car.Packages.Package.VehiclePrice.SellingPrice._amount || car.Packages.Package[0].VehiclePrice.SellingPrice._amount || car.Packages.Package.VehiclePrice.VehiclePrice.SellingPrice._amount) / xrate1).toLocaleString({ minimumFractionDigits: 2, maximumFractionDigits: 2 }, { style: 'currency', currency: currency });
                    carsInBranches.sortprice = car.Packages.Package.VehiclePrice.SellingPrice._amount || car.Packages.Package[0].VehiclePrice.SellingPrice._amount || car.Packages.Package.VehiclePrice.VehiclePrice.SellingPrice._amount;
                    carsInBranches.price5 = ((car.Packages.Package.VehiclePrice.SellingPrice._amount || car.Packages.Package[0].VehiclePrice.SellingPrice._amount || car.Packages.Package.VehiclePrice.VehiclePrice.SellingPrice._amount) / xrate1);
                    carsInBranches.rentalDays = car.Packages.Package.VehiclePrice._rentalDays || car.Packages.Package[0].VehiclePrice._rentalDays || car.Packages.Package.VehiclePrice.VehiclePrice._rentalDays;
                    carsInBranches.price1 = ((car.Packages.Package.VehiclePrice.SellingPrice._estimatedTotalAmount || car.Packages.Package[0].VehiclePrice.SellingPrice._estimatedTotalAmount || car.Packages.Package.VehiclePrice.VehiclePrice.SellingPrice._estimatedTotalAmount) / xrate1).toLocaleString({ minimumFractionDigits: 2, maximumFractionDigits: 2 }, { style: 'currency', currency: currency });
                    carsInBranches.lcurrency = car.Packages.Package.VehiclePrice._localCurrency || car.Packages.Package[0].VehiclePrice._localCurrency || car.Packages.Package.VehiclePrice.VehiclePrice._localCurrency;
                    carsInBranches.netprice = car.Packages.Package.VehiclePrice.NetPrice || car.Packages.Package[0].VehiclePrice._NetPrice || car.Packages.Package.VehiclePrice.VehiclePrice._NetPrice;
                    carsInBranches.paycurrency = car.Packages.Package.VehiclePrice._paymentCurrency || car.Packages.Package[0].VehiclePrice._paymentCurrency || car.Packages.Package.VehiclePrice.VehiclePrice._paymentCurrency;

                    carsInBranches.sellingprice = car.Packages.Package.VehiclePrice.SellingPrice || car.Packages.Package[0].VehiclePrice.SellingPrice || car.Packages.Package.VehiclePrice.VehiclePrice.SellingPrice;
                    carsInBranches.costPerDay2 = (Math.floor(carsInBranches.price5 / carsInBranches.rentalDays));

                    carsInBranches.costPerDay = Math.floor(carsInBranches.price5 / carsInBranches.rentalDays).toLocaleString({ minimumFractionDigits: 0, maximumFractionDigits: 0 }, { style: 'currency', currency: currency }).split('.')[0];;
                    carsInBranches.rateRef = car.Packages.Package.VehiclePrice.RateReference || car.Packages.Package[0].VehiclePrice.RateReference;
                }



                if (car.Packages.Package.Inclusions) {
                    carsInBranches.mileage = car.Packages.Package.Inclusions.Inclusion._name || car.Packages.Package.Inclusions.Inclusion["0"]._name || car.Packages.Package[0].Inclusions.Inclusion._name || car.Packages.Package[0].Inclusions.Inclusion["0"]._name;
                    if (car.Packages.Package.Inclusions.Inclusion["1"]) {
                        carsInBranches.incOne = car.Packages.Package.Inclusions.Inclusion["1"]._name || car.Packages.Package[0].Inclusions.Inclusion._name || car.Packages.Package[0].Inclusions.Inclusion["1"]._name;
                    }
                }
                //  if (car.Packages.Package[0].Inclusions.Inclusion[2]._name){ carsInBranches.incTwo = car.Packages.Package[0].Inclusions.Inclusion[2]._name};


                // if (car.Packages.Package.Inclusions.Inclusion[2]._name){ carsInBranches.incTwo =   car.Packages.Package.Inclusions.Inclusion[2]._name };

                function cloneObject(obj) {
                    let clone = {};
                    for (let i in obj) {
                        if (obj[i] != null && typeof(obj[i]) == "object")
                            clone[i] = cloneObject(obj[i]);
                        else
                            clone[i] = obj[i];
                    }
                    return clone;
                }
                // let copyCars = Object.assign({}, carsInBranches)
                let copyCars = cloneObject(carsInBranches)
                combinedVehicles.push(copyCars);
            }
        }
    }
    if (Array.isArray(availableBranches)) {
        for (let branch of availableBranches) {
            processBranch(branch);
        }
    } else {
        processBranch(availableBranches);
    }
    return combinedVehicles
}