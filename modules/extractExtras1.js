module.exports = (data, xrate) => {
    let exchangerate = data.Vehicle.VehiclePrice.SellingPrice.ExchangeRates.ExchangeRate._exchangeRate


    let extraInfo = data.Vehicle.Extras.Extra;
    let extraData = {};
    let extArray = [];
    let copyExtraData = {};
    if (Array.isArray(extraInfo)) {
        for (const extra of extraInfo) {
            (extra._otaCode == '511.DPB' || extra._otaCode == '449.PUM' || extra._otaCode == '963.QIY') ? extraData.EXTRA = 'CAEXTRA': extraData.EXTRA = 'SUPPLIER';

            if (extra._otaCode == '511.DPB') {
                extraData.docselector = "cancelpro"
            } else
            if (extra._otaCode == '449.PUM') {

                extraData.docselector = "CDW"
            } else if (extra._otaCode == '963.QIY') {
                extraData.docselector = "SProtector"
            } else {
                extraData.docselector = 'sel' + extra._otaCode;
            }



            extraData.otacode = extra._otaCode
            extraData.name = extra._name;
            extraData.url = (extra._otaCode !== '963.QIY') ? extra.Image._url : 'https://compareauto.herokuapp.com/Shield.png';

            extraData.name = extra._name;
            extraData.url = extra.Image._url || '../img/extra-placeholder.jpg'
            extraData.description = extra.Description;
            extraData.cost = extra.RentalPrice._amount;
            extraData.currency = extra.RentalPrice._currency;
            extraData.exchangeRate = exchangerate
            extraData.dollar = ((extraData.cost * exchangerate) / xrate).toFixed(2)
            copyExtraData = Object.assign({}, extraData);

            extArray.push(copyExtraData);
        }


    } else {

        (extraInfo._otaCode == '511.DPB' || extraInfo._otaCode == '449.PUM' || extraInfo._otaCode == '963.QIY') ? extraData.EXTRA = 'CAEXTRA': extraData.EXTRA = 'SUPPLIER';

        if (extraInfo._otaCode == '511.DPB') {
            extraData.docselector = "cancelpro"
        } else
        if (extraInfo._otaCode == '449.PUM') {

            extraData.docselector = "CDW"
        } else if (extraInfo._otaCode == '963.QIY') {
            extraData.docselector = "SProtector"
        } else {
            extraData.docselector = 'sel' + extraInfo._otaCode;
        }



        extraData.otacode = extraInfo._otaCode
        extraData.name = extraInfo._name;
        extraData.url = (extraInfo._otaCode !== '963.QIY') ? extraInfo.Image._url : 'https://compareauto.herokuapp.com/Shield.png';
        extraData.exchangeRate = exchangerate;
        extraData.name = extraInfo._name;
        extraData.url = extraInfo.Image._url || '../img/extra-placeholder.jpg'
        extraData.description = extraInfo.Description;
        extraData.cost = extraInfo.RentalPrice._amount;
        extraData.currency = extraInfo.RentalPrice._currency;
        extraData.dollar = ((extraData.cost * exchangerate) / xrate).toFixed(2)
        copyExtraData = Object.assign({}, extraData);

        extArray.push(copyExtraData);
    }


    return extArray;
}